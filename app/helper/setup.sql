DROP DATABASE IF EXISTS containerdb;
CREATE DATABASE containerdb;

USE containerdb;

DROP TABLE IF EXISTS content;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS feedback;
DROP TABLE IF EXISTS contactus;
DROP TABLE IF EXISTS faq;
DROP TABLE IF EXISTS food;
DROP TABLE IF EXISTS testimonial;
DROP TABLE IF EXISTS member;

CREATE TABLE member (

    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE,  -- new one
    password VARCHAR(255) NOT NULL, -- new one
    fname VARCHAR(255) NOT NULL,
    lname VARCHAR(255),
    level TINYINT(2) NOT NULL DEFAULT 1,  -- level 10:admin, 1:User, 5:VIP
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP, -- new one
    PRIMARY KEY (id)

) AUTO_INCREMENT=1;

CREATE TABLE testimonial (

    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    message TEXT NOT NULL,
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP, -- new one
    PRIMARY KEY (id)

) AUTO_INCREMENT=1;

CREATE TABLE food (

    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    rating_level FLOAT(2,1) UNSIGNED NOT NULL,
    rating_cnt INT UNSIGNED NOT NULL,
    views INT UNSIGNED NOT NULL,
    orders INT UNSIGNED NOT NULL,
    likes int(10) NOT NULL,             -- New one
    price FLOAT(5,2) UNSIGNED NOT NULL,
    f_desc TEXT NOT NULL,
    img_path VARCHAR(255) NOT NULL,
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP, -- new one
    PRIMARY KEY (id)

) AUTO_INCREMENT=1;

CREATE TABLE faq (

    id INT NOT NULL AUTO_INCREMENT,
    question VARCHAR(255) NOT NULL,
    answer TEXT NOT NULL,
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP, -- new one
    PRIMARY KEY (id)

) AUTO_INCREMENT=1;

CREATE TABLE contactus (

    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255),
    phone VARCHAR(15),
    purpose VARCHAR(20) NOT NULL,
    message TEXT NOT NULL,
    completed TINYINT(1) DEFAULT 0, -- new one
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP, -- new one
    PRIMARY KEY (id)

) AUTO_INCREMENT=1;

CREATE TABLE feedback (

    id INT NOT NULL AUTO_INCREMENT,
    satisfaction TINYINT(1) UNSIGNED,
    improvement TEXT,
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP, -- new one
    PRIMARY KEY (id)

) AUTO_INCREMENT=1;


-- New one
CREATE TABLE orders (
    id INT NOT NULL AUTO_INCREMENT,
    food_id INT NOT NULL,
    user_id INT NOT NULL,
    status TINYINT(1) DEFAULT 0, -- new one
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP, -- new one
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
            REFERENCES member(id)
            ON DELETE CASCADE,
    FOREIGN KEY (food_id)
            REFERENCES food(id)
            ON DELETE CASCADE
) AUTO_INCREMENT=1;


CREATE TABLE content (  -- new one

    id INT NOT NULL AUTO_INCREMENT,
    name varchar(30) NOT NULL,
    nth INT UNSIGNED NOT NULL DEFAULT 0,
    content VARCHAR(1000) NOT NULL,
    note VARCHAR(255) DEFAULT NULL,
    inPage VARCHAR(20) NULL DEFAULT NULL,
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)

) AUTO_INCREMENT=1;

-- *********************************
--          CREATE MEMBER 
-- *********************************
INSERT INTO member (email,password,fname,lname,level) VALUES ('user@user.com', 'user', 'Soffy', 'Test', 1);
INSERT INTO member (email,password,fname,lname,level) VALUES ('user1@user2.com', 'user1', 'Kay', 'Test', 5);
INSERT INTO member (email,password,fname,lname,level) VALUES ('user2@user2.com', 'user2', 'Korn', 'Test', 5);
INSERT INTO member (email,password,fname,lname,level) VALUES ('user3@user3.com', 'user3', 'Ted', 'Test', 1);
INSERT INTO member (email,password,fname,lname,level) VALUES ('admin@admin.com', 'admin', 'Sing', 'Test', 10);
INSERT INTO member (email,password,fname,lname,level) VALUES ('admin1@admin1.com', 'admin1', 'Jeff', 'Test', 10);
INSERT INTO member (email,password,fname,lname,level) VALUES ('admin2@admin2.com', 'admin2', 'Mik', 'Test', 10);
INSERT INTO member (email,password,fname,lname,level) VALUES ('admin3@admin3.com', 'admin3', 'Shoen', 'Test', 10);

INSERT INTO member (email,password,fname,lname,level) VALUES ('jeff@jeff.com', 'jeff', 'Jeff', 'Kranenburg', 1);
INSERT INTO member (email,password,fname,lname,level) VALUES ('steve@steve.com', 'steve', 'Steve', 'Stevenson', 1);
INSERT INTO member (email,password,fname,lname,level) VALUES ('roger@roger.com', 'steve', 'Roger', 'Rogers', 5);
INSERT INTO member (email,password,fname,lname,level) VALUES ('kevin@kevin.com', 'kevin', 'Kevin', 'Bateman', 5);
INSERT INTO member (email,password,fname,lname,level) VALUES ('sing@sing.com', 'sing', 'Sing', 'Cho',1);
INSERT INTO member (email,password,fname,lname,level) VALUES ('vinccent@vinccent.com', 'vinccent', 'Vinccent', 'Ning', 1);

INSERT INTO member (email,password,fname,lname,level) VALUES ('sunguk@sunguk.com', 'sunguk', 'Sunguk', 'Lee', 1);
INSERT INTO member (email,password,fname,lname,level) VALUES ('ian@ian.com', 'ian1', 'Ian', 'Mac', 1);
INSERT INTO member (email,password,fname,lname,level) VALUES ('choen@choen.toiohomai.ac.nz', 'choen', 'Choen', 'P', 5);
INSERT INTO member (email,password,fname,lname,level) VALUES ('nadia@nadia.com', 'nadia', 'nadia', 'J', 5);


/* SELECT * FROM member; */

-- *********************************
--       CREATE TESTIMONIAL 
-- *********************************

INSERT INTO testimonial (name,message) VALUES ('Jeff Kranenburg', 'I am very pleased with the service and the quality of food is very good. I recommend this place to all my friends.');
INSERT INTO testimonial (name,message) VALUES ('Alisha H.', 'I have visited too many Indian restaurants and this is my favorite by far. The food is amazing and worth the drive.');
INSERT INTO testimonial (name,message) VALUES ('Marcel N.', 'Very tasty food, reasonable prices. Atmosphere is special, but the food is great. Try the chicken tikka masala...');
INSERT INTO testimonial (name,message) VALUES ('Hotfood1', 'Excellent lunch buffet. really authentic and tasty when compared to other restaurants in MN.. I would go again. Pros: spicy and authentic food');
INSERT INTO testimonial (name,message) VALUES ('Vidya', "I love their buffet's, go early, and its fresh and hot, with lots of choices!!!! yum!!");
INSERT INTO testimonial (name,message) VALUES ('Lalit Meesala', 'Best Indian Restaurant in Twin Cities..... Good Service, Lot of varities...');

/* SELECT * FROM testimonial; */

-- *********************************
--           CREATE food
-- *********************************

INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Tubular Pasta', 4.2, 123, 200, 80, 100, 20.00, 
    'Tubular pastas are any pastas that are in the shape of a tube. They are available in many different sizes and shapes. Some tubes are long and narrow while others are short and wide. They are found with smooth or grooved exteriors. They are often served with a heavy sauce, but are also used in salads and casseroles.',
    '/imgs/food/pasta.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Petoncle Alfredo', 5.0, 63, 100, 143, 56, 15.00, 
    'Sliced grilled chicken and our signature, homemade alfredo sauce over fettuccine pasta.',
    '/imgs/food/petoncle-alfredo.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Chicken Salad', 3.5, 200, 454, 343, 196, 15.00, 
    'Fried chicken tenders, fresh Asian greens, rice noodles, almonds with an Oriental vinaigrette.', 
    '/imgs/food/chicken_salad.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Grilled Panini', 5.0, 235, 253, 214, 220, 12.00,
    'Grilled Vegetable Panini loaded with zucchini, bell peppers and red onion. Topped with mozzarella and an Italian herb olive oil spread. Quick vegetarian meal!',
    '/imgs/food/grilled_panini.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Seafood Salad', 4.8, 35, 89, 50, 60, 23.00, 
    'Toss roasted vegetables and salmon with a flavor-packed vinaigrette to serve on top of greens for a hearty dinner salad. For a twist, add a poached or fried egg on top. Serve with: Toasted whole-grain baguette and a glass of Riesling.',
    '/imgs/food/pasta.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Beef Steak', 4.1, 600, 1002, 876, 987, 32.00, 
    "Tender flat iron steak meets trendy Asian bistro flavor! With a side of Asian vegetable mix, it's an easy delicious dinner!", 
    '/imgs/food/steak.png');

INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Pizza', 5.0, 162, 312, 201, 299, 15.00, 
    'ender flat iron steak meets trendy Asian bistro flavor! With a side of Asian vegetable mix, it\'s an easy delicious dinner!',
    '/imgs/food/sipa.s-palvelut.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Smoked Pork Chops', 4.3, 90, 192, 78, 23, 20.00,
    'For a twist, add a poached or fried egg on top. Serve with: Toasted whole-grain baguette and a glass of Riesling.',
    '/imgs/food/tavuk-izgara.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Tomato Noodles', 4.4, 90, 134, 78, 50, 18.00,
    'Topped with mozzarella and an Italian herb olive oil spread. Quick vegetarian meal!',
    '/imgs/food/tomato_noodles.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Ziti Pasta', 4.5, 213, 302, 187, 190, 20.00,
    'With a side of Asian vegetable mix, it\'s an easy delicious dinner!',
    '/imgs/food/Ziti-plate-above.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Smoked Salmon Salad', 4.8, 423, 480, 389, 412, 10.00,
    'Some tubes are long and narrow while others are short and wide. ',
    '/imgs/food/Smoked_Salmon_Salad.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Grilled Pork & Mango Juice', 3.9, 182, 515, 126, 98, 15.00,
    'They are often served with a heavy sauce, but are also used in salads and casseroles.',
    '/imgs/food/grilled_pork_and_juice.png');
INSERT INTO food (name,rating_level,rating_cnt,views,orders,likes,price,f_desc,img_path)
    VALUES ('Pizza Romana', 3.5, 52, 104, 36, 40, 15.00,
    'Toasted whole-grain baguette and a glass of Riesling.',
    '/imgs/food/pizza_romana2.png');

-- SELECT * FROM food;

-- *********************************
--       CREATE FAQ 
-- *********************************

INSERT INTO faq (question,answer) VALUES ("I have just placed an order, when will I get my delivery?", "We charge one week in advance. Payments are taken on Sunday for delivery the following Sunday. For example, if you order on Wednesday, your first payment will be processed on the Sunday with your first delivery arriving on the following Sunday.");
INSERT INTO faq (question,answer) VALUES ("I have missed the Sunday cut off, can I still get a box this week?", "All of our orders are placed with our suppliers on Monday, and then the hard work of getting the boxes ready for delivery starts. However, it's worth sending us an email to see if we can squeeze you in.");
INSERT INTO faq (question,answer) VALUES ("When do I get charged?","We charge one week in advance, payments are taken on Sunday for delivery the following Sunday.");
INSERT INTO faq (question,answer) VALUES ("What time does my Green Dinner Table box arrive?", "All deliveries are between 3 pm and 6:30 pm on Sunday. If you haven't received your box by 6:30 pm please get in touch with us ASAP.");
INSERT INTO faq (question,answer) VALUES ("Something is missing from my box?", "All Green Dinner Table boxes are packed lovingly by hand, and while rare, sometimes we might miss an item from a box. If this happened, please get in contact ASAP.");
INSERT INTO faq (question,answer) VALUES ("Can you do gluten-free boxes?","Yes! Please email us at info@greendinnertable.co.nz for more info. We are not a gluten-free kitchen, but we will replace items to accommodate for the needs of our gluten-free customers.");
INSERT INTO faq (question,answer) VALUES ("How does HaveThere work?", "It's easy! Enter your address, select a restaurant and make your order. Then enter your address (either create an account or just give us your details), pay for your meal by credit/debit card and sit back and wait for us to knock on the door with your dinner.");
INSERT INTO faq (question,answer) VALUES ("What kind of restaurants are listed on HaveThere?", "We partner with your favourite restaurants to deliver top quality cuisine.");
INSERT INTO faq (question,answer) VALUES ("How is the food delivered to me?","One of our speedy drivers will deliver your meal on their noble steed (or motorised scooter). Don't worry, we'll keep it nice and hot.");
INSERT INTO faq (question,answer) VALUES ("What if something is wrong with my order?", "Contact us to let us know and we will do our best to sort it with the restaurant.");
INSERT INTO faq (question,answer) VALUES ("What if my order is late?", "Some orders may take a little longer than average during busy periods. We'll do our best to keep you up to date with an ETA should anything drastic happen. If you are unsatisfied, let us know by emailing support@HaveThere.co.nz and we can discuss a refund of your delivery fee.");
INSERT INTO faq (question,answer) VALUES ("Why are not you delivering to my area yet?", "We will be! Watch this space. Give us the hurry up by emailing info@HaveThere.co.nz and demanding we deliver to you ASAP!");

-- SELECT * FROM faq;

-- *********************************
--       CREATE CONTACTUS 
-- *********************************

INSERT INTO contactus (name,email,phone,purpose,message) VALUES ('Jeff Kranenburg', 'jeff@abc.com', '0210343043', 'claim', 'I think I got a wrong receipt. Can you refund it?');
INSERT INTO contactus (name,email,phone,purpose,message) VALUES ('Steve Stevenson', 'steve@abc.com', '073948374', 'query', 'Can you deliver to rural area?');
INSERT INTO contactus (name,email,phone,purpose,message) VALUES ('Roger .K', 'roger@abc.com', '02984755758', 'query', 'How long does it take?');
INSERT INTO contactus (name,email,phone,purpose,message) VALUES ('Kevin Bateman', 'kevin@abc.com', '02983757384', 'other', "Are you free? I'd like to meet and have dinner with you tonight. Can I?");
INSERT INTO contactus (name,email,phone,purpose,message) VALUES ('Sing Cho', 'sing@abc.com', '02185628487', 'claim', "Your service is awful. Food is terrible!!");
INSERT INTO contactus (name,email,phone,purpose,message) VALUES ('Vinccent', 'vinncent@abc.com', '0218473939', 'query', 'What time do you close?');

-- SELECT * FROM contactus;

-- *********************************
--       CREATE FEEDBACK 
-- *********************************

INSERT INTO feedback (satisfaction,improvement) VALUES (5, NULL);
INSERT INTO feedback (satisfaction,improvement) VALUES (4, 'Please, give him smile when you face a customer.');
INSERT INTO feedback (satisfaction,improvement) VALUES (1, 'There is no way to improve your service.');
INSERT INTO feedback (satisfaction,improvement) VALUES (4, NULL);
INSERT INTO feedback (satisfaction,improvement) VALUES (4, NULL);
INSERT INTO feedback (satisfaction,improvement) VALUES (5, NULL);

-- SELECT * FROM feedback;

-- *********************************
--       Additional Pages
-- *********************************

-- Terms & Conditions

INSERT INTO content (name, nth, content) VALUES ('terms', 0,
    "<b>Registration</b><br>
    By signing up to our website and purchasing products from East Neuk Cooks, you are agreeing to the terms and conditions of sale,
    which apply to all transactions on eastneukcooks.com signing up to the service means we must have the following information:<br>
    - Your address, including the postcode of the billing address associated with the payment card.<br>
    - Your home telephone number<br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 1,
    "- Your email address, so we can supply you with important information such as your order confirmation and changes to the East Neuk Cook Home Delivery Service<br>
    East Neuk Cooks only sells goods on our website to the end user and they should not, in any way, be resold without our prior written consent.
    There shall be two stages of order confirmation. The first shall be the 'on screen confirmation' immediately after the payment has been received,"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 2,
    "and the second shall be an email to confirm your full order details. We do not hold your payment details from any previous orders.
    Therefore, every purchase at eastneukcooks.com shall require new payment details entered. It is your responsibility to keep your password secure.<br>
    <br><br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 3,
    "<b>Product Prices</b><br>
    The price of goods charged shall be as quoted on the web site at the time you confirm your order with us. Excluding any inadvertent technical error on pricing,
    we will honour any prices as published at the time of placing your order. Your debit/credit card shall only be charged for items dispatched to you."
);
INSERT INTO content (name, nth, content) VALUES ('terms', 4,
    "All of our food products are zero-rated for VAT. Prices of goods do not include the delivery charge. We do endeavour to keep to the ingredients in the menu description
    but things are sometimes beyond our control when products become unavailable, we reserve the right to make certain changes to the menu if this is the case.<br>
    Gift Vouchers<br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 5,
    "1. Vouchers cannot be exchanged or refunded for cash.<br>
    2. Vouchers can be used towards the purchase of goods at a higher price than their face value upon payment of the difference. No change or credit will be given for unused portions of a voucher.<br>
    4. East Neuk Cooks is under no liability for replacement or refund should vouchers be lost, stolen, damaged or destroyed whether they have been redeemed or not.<br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 6,
    "5. Vouchers are only valid for the amount printed next to it.<br>
    <br>
    <b>Delivery</b><br>
    Free delivery is offered on all orders over $20.00. Our minimum order for delivery is $10.00. Meals are delivered free within a 5 mile radius of Cupar,
    if you live out-with this area there is a $3.00 delivery charge. We deliver personally to your home between 8am & 8pm Monday to Sunday;"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 7,
    "you will be given an option of available delivery dates in your location/area sent to you by email after checkout and payment. <br>
    We make all our food fresh (apart from our single meal packages) so all orders placed will be given a delivery choice a minimum of 5 days from the order date.<br>
    You do need to be in to receive your delivery because the food will not arrive in insulated packaging"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 8,
    "If you are unable to be at home we are happy to leave with a neighbour or friend within a mile of your home; you just need to call us with the details. <br>
    If you call us on the day, we will also be able to give you an approximate time window for delivery. <br>
    We reserve the right to restrict deliveries in certain areas and this includes the right to withdraw the service altogether, depending on the demand on our kitchen.<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 9,
    "<b>Cancelling an order</b><br>
    Once full payment has been made it will not be refunded if you cancel your order [or place an order] within 3 days of your requested delivery date.<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 10,
    "<b>Our refund policy</b><br>
    If you are unsatisfied with any of the items in your order or any part of the delivery service from East Neuk Cooks please contact us 
    - either by emailing us via the Contact Us page of the website or by telephoning 07969 208 497.
    If you would like to make a comment or recommendation regarding our food, then please visit the Contact Us page of the website and select 'Contact Us'."
);
INSERT INTO content (name, nth, content) VALUES ('terms', 11,
    "As we sell perishable goods, we ask that you - the customer - inspect the food once you have opened the box.
    If you are unhappy with the goods, please contact us immediately on 07969 208 497. We shall be happy to exchange or refund any item that has been damaged in transit.
    If we have sent you an incorrect item, please notify us as soon as possible. If you would like us to replace the incorrect item with the item that you ordered,"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 12,
    "we shall send you the correct one as soon as possible. Obviously we shall not charge you for incorrect item.<br>
    <br>
    We operate an open and honest policy at East Neuk Cooks and will work our hardest to make sure we can accommodate and act on any problems or suggestions.<br>
    <br><br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 13,
    "<b>Payment Methods</b><br>
    When using our shopping basket there are several payment options on checkout, you can choose to pay either by Debit or Credit Card through our secure Worldpay payment gateway or
    if you would prefer to pay by Cheque or Direct Bank Transfer this can also be arranged by emailing us at orders@eastneukcooks.com and we can process your order manually."
);
INSERT INTO content (name, nth, content) VALUES ('terms', 14,
    "If you are paying by bank transfer or Cheque the funds must be in our account 24hours prior to your requested delivery date.<br>
    <br>
    <br>
    If you want to pay by Cheque it will only be possible if your delivery dates is 2 weeks or more from the order date. <br>
    If the payment has not cleared in our account we reserve the right to cancel the delivery.<br>
    <br><br><br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 15,
    "<b>Terms and Conditions:</b><br>
    <br>
    <b>Outside Catering - Finger and Fork Buffet</b><br>
    <br>
    The following conditions will constitute a contract between East Neuk Cooks and the customer making the reservation:<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 16,
    "<b>Booking Catering</b><br>
    <br>
    Please book a minimum of 14 days in advance to allow for sufficient preparation time, possibly including a meeting to discuss your requirements.
    A deposit of 30% is required to secure a booking; final numbers need to be confirmed 7 days prior to the event.<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 17,
    "<b>Dietary requirements</b><br>
    <br>
    Prior notice must be given including any allergies which we will do our best to cater for.<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 18,
    "<b>Deposits and Payments</b><br>
    <br>
    A Deposit of 30% is required to secure a booking, payment of the deposit signifies acceptance of the following 'Terms and Conditions'.
    Quotations are valid for 10 days from the date of issue.
    We reserve the right to adjust unconfirmed quotations beyond 10 days if supplier's costs increase.
    Quotations are free and provided without obligation.<br>
    <br>
    Nothing in these Terms and Conditions affects your Statutory Rights<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 19,
    "<b>Cancellation</b><br>
    <br>
    The following cancellation charges will apply once the booking has been confirmed, together with the non-refundable deposit. Cancellations must be confirmed by email.<br>
    <br>
    Within 10 Days of Booking Date - Deposit Only<br>
    <br>
    Within 5 Days of Booking Date - Deposit & 50% of quoted price<br>
    <br>
    Within 3 Days of Booking Date - Full Quoted Price.<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 20,
    "<b>Delivery</b><br>
    <br>
    Delivery is free within a 10 mile radius of Cupar, if you live out-with this area there is a $3.00 delivery charge.<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 21,
    "<b>Staff</b><br>
    <br>
    We do not supply service staff as we offer a delivery service only, all our food will be delivered in disposable packaging but we can arrange to collect your own dishware to use if preferred. This can be arranged during your booking with us.<br>
    <br>"
);
INSERT INTO content (name, nth, content) VALUES ('terms', 22,
    "<b>Catering Numbers</b><br>
    <br>
    Final numbers must be notified not less than 10 days before the delivery date. Where numbers exceed the original quote they will be charged pro-rata. Where increased numbers are notified less than 36 hours ahead of the function the availability of the extra food cannot be guaranteed.<br>"
);

-- About Us
INSERT INTO content (name, nth, content, note, inPage) VALUES ('aboutus', 0,
    "
    ",
    "CEO Micelle", "aboutus");
INSERT INTO content (name, nth, content, note, inPage) VALUES ('ceo', 1,
    "With years of restaurant experience under our woefully well-fitting belts, 
    we decided to do something about it and create HaveThere. 
    HaveThere is the last place you'll ever have to go online to order yummy cuisine for delivery 
    or take-out.
    <img src='/imgs/ceo.png'>",
    "CEO Micelle", "aboutus");
INSERT INTO content (name, nth, content, note, inPage) VALUES ('ceo', 2,
    "We've wrangled up a fleet of environmentally friendly vehicles to get fresh food 
    from the restaurant straight to your door, quickly and hassle free!",
    "CEO Micelle", "aboutus");
INSERT INTO content (name, nth, content, note, inPage) VALUES ('ceo', 3,
    "We're excited. We hope you are too!
    <p class='ceo'>CEO Micelle</p>",
    "CEO Micelle", "aboutus");
-- Images in 'About' page
INSERT INTO content (name, content, note, inPage) VALUES ('image', "/imgs/chef1.png", "chief1", "aboutus");
INSERT INTO content (name, content, note, inPage) VALUES ('image', "/imgs/chef2.png", "chief2", "aboutus");
INSERT INTO content (name, content, note, inPage) VALUES ('image', "/imgs/staff.jpg", "staff", "aboutus");
INSERT INTO content (name, content, note, inPage) VALUES ('image', "/imgs/grand_open.png", "post", "aboutus");
-- Images in 'About' page
INSERT INTO content (name, content, note, inPage) VALUES ('textChef', "So long as you have food in your mouth you have solved all questions for the time being.", "chief1", "aboutus");
INSERT INTO content (name, content, note, inPage) VALUES ('nameChef', "Tanya Blazelist.", "chief1", "aboutus");
INSERT INTO content (name, content, note, inPage) VALUES ('titleChef', "Master Chef", "chief1", "aboutus");
INSERT INTO content (name, content, note, inPage) VALUES ('textChef', "Only the pure in heart can make a good soup.", "chief2", "aboutus");
INSERT INTO content (name, content, note, inPage) VALUES ('nameChef', "Laura Mercier.", "chief2", "aboutus");
INSERT INTO content (name, content, note, inPage) VALUES ('titleChef', "Master Chef", "chief2", "aboutus");

-- External links page
INSERT INTO content (name, content, note) VALUES ('link', "https://www.vonwheels.com/", "V On Wheels");
INSERT INTO content (name, content, note) VALUES ('link', "https://www.menulog.co.nz/browse/locations/tauranga/", "MENULOG");
INSERT INTO content (name, content, note) VALUES ('link', "http://houseofindia.co.nz/", "House of India");
INSERT INTO content (name, content, note) VALUES ('link', "http://ownmasala.co.nz/", "Own Masala Indian Restaurant ");
INSERT INTO content (name, content, note) VALUES ('link', "http://www.kfc.co.nz/delivery/", "KFC");
INSERT INTO content (name, content, note) VALUES ('link', "https://emmasfoodbag.co.nz/", "Emma’s Food Bag");
INSERT INTO content (name, content, note) VALUES ('link', "https://queenstowninsider.com/food-delivery/", "Insider");
INSERT INTO content (name, content, note) VALUES ('link', "https://www.doordash.com", "Doordash");
INSERT INTO content (name, content, note) VALUES ('link', "https://www.foodora.de/en/city/karlsruhe", "Foodora");

