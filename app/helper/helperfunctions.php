<?php

    function getArrayFromURL() {

        if(isset($_GET['url'])) {

            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            return $url;

        }

    }

    function myTryParse($url) {

        $getURLData = getArrayFromURL($url);

        array_splice($getURLData, 0, 2);
        $isNumber = intval($getURLData[0]);

        if($isNumber) {
            return $getURLData[0]; 
        } else {
            return 0;
        }
    }

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


    function checkEmail($email)
    {
        if (empty($email))
        {
            $website = "";
            return false;
        }
        else
        {
            $website = test_input($email);
            
            // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website))
            {
                return false; 
            }
            
            return true;
        }
    }

    function preventInjection_Stripslashes()
    {
        // -------------------------------
        // Check if magic_quotes is ON
        // -------------------------------
        // If the directive magic_quotes_sybase is ON it will completely override magic_quotes_gpc.
        // So even when get_magic_quotes() returns TRUE neither double quotes,
        // backslashes or NUL's will be escaped. Only single quotes will be escaped.
        // In this case they'll look like: ''
        if (get_magic_quotes_gpc())
        {
            foreach ($_POST as $key => $value)
            {
                $_POST[$key] = stripslashes($value);
            }
        }
    }

    function preventInjection_TrimStrip()
    {
        foreach ($_POST as $key => $value)
        {
            $_POST[$key] = strip_tags(trim($value));
        }
    }

?>