<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Feedback extends Controller {

        public function __construct() {
            $this->model = $this->model('Feedback');
            $this->modelFood = $this->model('Food');
            $this->modelTestimonial = $this->model('Testimonial');
        }

        //-------------------------
        // Show Feedback page
        //-------------------------
        public function index()
        {
            $data = [
                'page' => 'feedback',
                'template' => 'template4',
                'awesomeFont' => true
            ];

            // Check & Set Login Session
            session_start();
            $this->getSessionVar($data);

            // Favorite Food
            $favorites = $data['favorites'] = $this->modelFood->getFavoriteFoods(6);

            if(!isset($favorites) && count($favorites) <= 0)
            {
                $data['favorites'] = null;
            }

            // Testimonial
            $testimonial = $data['testimonial'] = $this->modelTestimonial->getTestimonial();

            if(!isset($testimonial) && count($testimonial) != 1)
            {
                $data['testimonial'] = null;
            }

            $this->view('pages/feedback', $data);
        }

        //-------------------------
        // Set Feedback
        //-------------------------
        public function ajax_setFeedback()
        {
            $isValid = true;

            //---------------------------------------
            // Prevent Injection Attact Step1 : Strip slashes
            //---------------------------------------
            preventInjection_Stripslashes();
            preventInjection_TrimStrip();

            //---------------------------------------
            // Validation
            //---------------------------------------
            (isset($_POST['satisfaction']) && !empty($_POST['satisfaction'])) ? $satisfaction = $_POST['satisfaction'] : $isValid = false;
            (isset($_POST['improvement']) && !empty($_POST['improvement'])) ? $improvement = $_POST['improvement'] : $isValid = false;

            if($isValid == false)
            {
                echo '[{"success":false,"error":"Wrong Parameter"}]';
                exit;
            }

            // Set a feedback
            $result = $this->model->setFeedback($satisfaction, $improvement);


            if($result == true)
            {
                echo '[{"success":true}]';
            }
            else
            {
                echo '[{"success":false, "error":"DB Query Error"}]';
            }
        }

    }

?>