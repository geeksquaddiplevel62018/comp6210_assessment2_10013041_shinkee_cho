<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class FAQ extends Controller {

        public function __construct() {
            $this->modelFAQ = $this->model('FAQ');
            $this->modelFood = $this->model('Food');
            $this->modelTestimonial = $this->model('Testimonial');
        }

        //-------------------------
        // Show FAQ page
        //-------------------------
        public function index()
        {
            $data = [
                'page' => 'faq',
                'template' => 'template4',
                'awesomeFont' => true,
                'bootstrapCSS' => true,
                'bootstrapJS' => true
            ];

            // Check & Set Login Session
            session_start();
            $this->getSessionVar($data);

            // FAQ List
            $faqList = $data['faqList'] = $this->modelFAQ->getFAQ();

            if(!isset($faqList) && count($faqList) <= 0)
            {
                $data['recommend'] = null;
            }

            // Favorite Food
            $favorites = $data['favorites'] = $this->modelFood->getFavoriteFoods(6);

            if(!isset($favorites) && count($favorites) <= 0)
            {
                $data['favorites'] = null;
            }

            // Testimonial
            $testimonial = $data['testimonial'] = $this->modelTestimonial->getTestimonial();

            if(!isset($testimonial) && count($testimonial) != 1)
            {
                $data['testimonial'] = null;
            }

            $this->view('pages/faq', $data);
        }
    }

?>