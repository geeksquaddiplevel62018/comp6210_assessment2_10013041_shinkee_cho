<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class About extends Controller {

        public function __construct() {
            $this->model = $this->model('AdditionalPages');
        }

        //---------------------------------------
        // About
        //---------------------------------------
        public function index()
        {
            $data = [
                'page' => 'about',
                'template' => 'template2'
            ];

            // Check & Set Login Session
            session_start();
            $this->getSessionVar($data);

            //
            // Terms & Conditions
            $aboutus = $data['aboutus'] = $this->model->getAboutus();

            if(!isset($aboutus) && count($aboutus) <= 0)
            {
                $data['aboutus'] = null;
            }
            
            $this->view('pages/about', $data);
        }
    }

?>