<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Member extends Controller {

        public function __construct() {
            $this->model = $this->model('Member');
            $this->modelFood = $this->model('Food');
            $this->modelTestimonial = $this->model('Testimonial');
        }
        
        // Redirect to login page
        public function index(){ $this->login(); }

        //-------------------------
        // Show Login page
        //-------------------------
        public function login()
        {
            $data = [
                'page' => 'login',
                'template' => 'template4',
                'awesomeFont' => true
            ];

            // Check & Set Login Session
            session_start();
            $this->getSessionVar($data);

            // Favorite Food
            $favorites = $data['favorites'] = $this->modelFood->getFavoriteFoods(6);

            if(!isset($favorites) && count($favorites) <= 0)
            {
                $data['favorites'] = null;
            }

            // Testimonial
            $testimonial = $data['testimonial'] = $this->modelTestimonial->getTestimonial();

            if(!isset($testimonial) && count($testimonial) != 1)
            {
                $data['testimonial'] = null;
            }

            $this->view('pages/login', $data);
        }

        //-------------------------
        // Login with email & password
        //-------------------------
        public function ajax_login()
        {
            $isValid = true;  

            //---------------------------------------
            // Prevent Injection Attact Step1 : Strip slashes
            //---------------------------------------
            preventInjection_Stripslashes();
            preventInjection_TrimStrip();

            //---------------------------------------
            // Validation
            //---------------------------------------
            (isset($_POST['email']) && !empty($_POST['email'])) ? $email = $_POST['email'] : $isValid = false;
            (isset($_POST['password']) && !empty($_POST['password'])) ? $password = $_POST['password'] :  $isValid = false;

            if($isValid == false)
            {
                echo '[{"success":false,"error":"Wrong Parameter"}]';
                exit;
            }

            //---------------------------------------
            // Email & Password Validation
            //---------------------------------------
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) || strlen($password) < 4)
            {
                echo '[{"success":false,"error":"Your id or password is wrong! Please check it"}]'; 
            }

            // Get user info
            $memberInfo = $this->model->getMember($email);
            
            // Verify a user password to login
            if(isset($memberInfo) && count($memberInfo) == 1)
            {
                if($memberInfo[0]['password'] == $password)
                {
                    session_start();
                    session_unset();

                    $this->setSessionVar($memberInfo[0]);

                    $memberInfo[0]['success'] = true;

                    echo json_encode($memberInfo);
                }
                else
                {
                    echo '[{"success":false,"error":"Wrong Password"}]';
                }
            } 
            else
            {
                echo '[{"success":false, "error":"Your id or password is wrong! Please check it"}]';
            }
        }

        //---------------------------------------
        // Logout
        //---------------------------------------
        public function logout()
        {
            session_start();

            session_unset(); 

            // destroy the session 
            //session_destroy(); 

            $data = [
                'page' => 'home',
                'template' => 'template1',
                'awesomeFont' => true
            ];

            $this->getSessionVar($data);

            $this->redirect();
        }


        //---------------------------------------
        // Sign up
        //---------------------------------------
        public function signup()
        {
            $data = [
                'page' => 'signup',
                'template' => 'template4',
                'awesomeFont' => true
            ];

            session_start();
            $this->getSessionVar($data);
            
            // Favorite Food
            $favorites = $data['favorites'] = $this->modelFood->getFavoriteFoods(6);

            if(!isset($favorites) && count($favorites) <= 0)
            {
                $data['favorites'] = null;
            }

            // Testimonial
            $testimonial = $data['testimonial'] = $this->modelTestimonial->getTestimonial();

            if(!isset($testimonial) && count($testimonial) != 1)
            {
                $data['testimonial'] = null;
            }

            // Terms & Conditions
            $terms = $data['terms'] = $this->model->getTermsConditions();

            if(!isset($terms) && count($terms) <= 0)
            {
                $data['terms'] = null;
            }


            $this->view('pages/signup', $data);
        }

        //-------------------------
        // Add a member
        //-------------------------
        public function ajax_addMember($param)
        {
            $isValid = true;

            //---------------------------------------
            // Prevent Injection Attact Step1 : Strip slashes
            //---------------------------------------
            preventInjection_Stripslashes();
            preventInjection_TrimStrip();

            //---------------------------------------
            // Validation
            //---------------------------------------
            (isset($_POST['email']) && !empty($_POST['email'])) ? $email = $_POST['email'] : $isValid = false;
            (isset($_POST['password']) && !empty($_POST['password'])) ? $password = $_POST['password'] : $isValid = false;
            (isset($_POST['fname']) && !empty($_POST['fname'])) ? $fname = $_POST['fname'] : $isValid = false;
            (isset($_POST['lname']) && !empty($_POST['lname'])) ? $lname = $_POST['lname'] : $isValid = false;
            $level = (isset($_POST['level']) && !empty($_POST['level'])) ? $_POST['level'] : 1;

            if($isValid == false)
            {
                echo '[{"success":false,"error":"Wrong Parameter"}]';
                exit;
            }


            $result = $this->model->addMember($email, $password, $fname, $lname, $level);

            // Set login session & return
            if($result == true)
            {
                // Get user info
                $memberInfo = $this->model->getMember($email);
                
                // Verify a user password to login
                if(isset($memberInfo) && count($memberInfo) == 1)
                {
                    if($memberInfo[0]['password'] == $password)
                    {
                        session_start();
                        session_unset();

                        $this->setSessionVar($memberInfo[0]);

                        $memberInfo[0]['success'] = true;
                        echo json_encode($memberInfo);
                    }
                    else
                    {
                        echo '[{"success":false,"error":"Wrong Password"}]';
                    }
                } 
                else
                {
                    echo '[{"success":false, "error":"Your id or password is wrong! Please check it"}]';
                }
            }
            else
            {
                echo '[{"success":false, "error":"DB Query Error"}]';
            }

            
        }
    }

?>