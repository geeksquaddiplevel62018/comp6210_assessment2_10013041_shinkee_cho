<?php

include(APPROOT . '/helper/helperfunctions.php');

class MyPage extends Controller {

    public function __construct() {
        $this->model = $this->model('MyPage');
    }

    // Redirect
    public function index(){ $this->myinfoTab(); }

    //-------------------------
    // Show MyPage Tab for a member
    //-------------------------
    public function myinfoTab()
    {
        $data = [
            'page' => 'mypage',
            'template' => 'template5',
            'awesomeFont' => true,
            'tab' => 'myinfo'
        ];

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);


        if($data['isLoggedin'])
        {
            $myInfo = $this->model->getMyInfo($_SESSION['email']);

            if(isset($myInfo) && count($myInfo) == 1)
            {
                $data['myinfo'] = $myInfo[0];
            } 
            else
            {
                $data['myinfo'] = null;
            }

            $this->view('pages/mypage', $data);
        }
        else
        {
            $this->redirect(URLROOT."Member/login");
        }
    }

    //-------------------------
    // Show Orders Tab for a member
    //-------------------------
    public function ordersTab()
    {
        $data = [
            'page' => 'mypage',
            'template' => 'template5',
            'awesomeFont' => true,
            'tab' => 'orders'
        ];

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $orders = $this->model->getOrders($data['userid']);

            if(!empty($orders) && count($orders) > 0)
            {
                $data['orders'] = $orders;
            } 
            else
            {
                $data['orders'] = null;
            }

            $this->view('pages/mypage', $data);
        }
        else
        {
            $this->redirect(URLROOT."Member/login");
        }
    }

    //---------------------------------------
    // (Ajax) Cancel an order
    //---------------------------------------
    public function ajaxCancelOrder()
    {

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $orderid = -1;

            //---------------------------------------
            // Prevent Injection Attact Step1 : Strip slashes
            //---------------------------------------
            preventInjection_Stripslashes();
            preventInjection_TrimStrip();

            //---------------------------------------
            // Validation
            //---------------------------------------
            if(isset($_POST['orderid']) && !empty($_POST['orderid']) && is_numeric($_POST['orderid']))
            {
                $orderid = $_POST['orderid'];
            }
            else
            {
                echo '[{"success":false,"error":"Wrong Parameter"}]';
                exit;
            }

            // Complete the delivery
            $result = $this->model->updateStatus($orderid, 1);

            if($result == true)
            {
                echo '[{"success":true}]';
            }
            else
            {
                echo '[{"success":false, "error":"DB Query Error"}]';
            }
        }
        else
        {
            echo '[{"success":false, "error":"Are you hacking my site? or Please, logout and login, and then try it again."}]';
        }
    }

    //---------------------------------------
    // (Ajax) Remove an order
    //---------------------------------------
    public function ajaxRemoveOrder()
    {
        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $orderid = -1;

            //---------------------------------------
            // Prevent Injection Attact Step1 : Strip slashes
            //---------------------------------------
            preventInjection_Stripslashes();
            preventInjection_TrimStrip();

            //---------------------------------------
            // Validation
            //---------------------------------------
            if(isset($_POST['orderid']) && !empty($_POST['orderid']) && is_numeric($_POST['orderid']))
            {
                $orderid = $_POST['orderid'];
            }
            else
            {
                echo '[{"success":false,"error":"Wrong Parameter"}]';
                exit;
            }

            // Complete the delivery
            $result = $this->model->updateStatus($orderid, 3);

            if($result == true)
            {
                echo '[{"success":true}]';
            }
            else
            {
                echo '[{"success":false, "error":"DB Query Error"}]';
            }
        }
        else
        {
            echo '[{"success":false, "error":"Are you hacking my site? or Please, logout and login, and then try it again."}]';
        }
    }
}

?>