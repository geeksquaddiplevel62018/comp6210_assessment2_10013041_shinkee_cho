<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class ResetDB extends Controller {

        public function __construct() {
            $this->model = $this->model('AdditionalPages');
        }

        //---------------------------------------
        // Reset Database
        //---------------------------------------
        public function index()
        {
            $sql_file = APPROOT.'/helper/setup.sql';

            $contents = file_get_contents($sql_file);

            // Remove C style and inline comments
            $comment_patterns = array('/\/\*.*(\n)*.*(\*\/)?/', //C comments
                                    '/\s*--.*\n/', //inline comments start with --
                                    '/\s*#.*\n/', //inline comments start with #
                                    );
            $contents = preg_replace($comment_patterns, "\n", $contents);

            //Retrieve sql statements
            $statements = explode(";\n", $contents);
            $statements = preg_replace("/\s/", ' ', $statements);

            $dbClass = '\Database'; 
            $db = new $dbClass;

            foreach ($statements as $query)
            {
                if (trim($query) != '')
                {
                    $db->query($query);
                    $res = $db->execute();

                    if(!$res)
                    {
                        echo "error";
                    }
                }
            }

            $this->view('pages/resetDB', $data);
        }
    }

?>