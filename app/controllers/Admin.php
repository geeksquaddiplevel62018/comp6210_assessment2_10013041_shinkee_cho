<?php

include(APPROOT . '/helper/helperfunctions.php');

class Admin extends Controller {

    public function __construct() {
        $this->model = $this->model('Admin');
    }

    // Redirect
    public function index(){ $this->dashboard(); }


    //---------------------------------------
    // My Info
    //---------------------------------------
    public function dashboard()
    {
        $data = [
            'page' => 'admin',
            'template' => 'template5',
            'awesomeFont' => true,
            'tab' => 'myinfo'
        ];

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $adminInfo = $this->model->getMyInfo($_SESSION['email']);

            if(!empty($adminInfo) && count($adminInfo) == 1)
            {
                $data['myinfo'] = $adminInfo[0];
            } 
            else
            {
                $data['myinfo'] = null;
            }

            $this->view('pages/admin', $data);
        }
        else
        {
            $this->redirect(URLROOT."Member/login");
        }
    }


    //---------------------------------------
    // Contact us
    //---------------------------------------
    public function contactus()
    {
        $data = [
            'page' => 'admin',
            'template' => 'template5',
            'awesomeFont' => true,
            'tab' => 'contactus'
        ];

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $contactus = $this->model->getContactus();

            if(!empty($contactus) && count($contactus) > 0)
            {
                $data['contactus'] = $contactus;
            } 
            else
            {
                $data['contactus'] = null;
            }

            $this->view('pages/admin', $data);
        }
        else
        {
            $this->redirect(URLROOT."Member/login");
        }
    }


    //---------------------------------------
    // Feedback
    //---------------------------------------

    public function feedback()
    {
        $data = [
            'page' => 'admin',
            'template' => 'template5',
            'awesomeFont' => true,
            'tab' => 'feedback'
        ];

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);


        if($data['isLoggedin'])
        {
            $feedback = $this->model->getFeedback();

            if(!empty($feedback) && count($feedback) > 0)
            {
                $data['feedback'] = $feedback;
            } 
            else
            {
                $data['feedback'] = null;
            }

            $this->view('pages/admin', $data);
        }
        else
        {
            $this->redirect(URLROOT."Member/login");
        }
    }

    //---------------------------------------
    // FAQ
    //---------------------------------------
    public function faq()
    {
        $data = [
            'page' => 'admin',
            'template' => 'template5',
            'awesomeFont' => true,
            'tab' => 'faq'
        ];

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $faq = $this->model->getFAQ();

            if(!empty($faq) && count($faq) > 0)
            {
                $data['faq'] = $faq;
            } 
            else
            {
                $data['faq'] = null;
            }

            $this->view('pages/admin', $data);
        }
        else
        {
            $this->redirect(URLROOT."Member/login");
        }
    }


    //---------------------------------------
    // Member
    //---------------------------------------
    public function member()
    {
        $data = [
            'page' => 'admin',
            'template' => 'template5',
            'awesomeFont' => true,
            'tab' => 'member'
        ];

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $member = $this->model->getMember();

            if(!empty($member)  && count($member) > 0)
            {
                $data['member'] = $member;
            } 
            else
            {
                $data['member'] = null;
            }

            $this->view('pages/admin', $data);
        }
        else
        {
            $this->redirect(URLROOT."Member/login");
        }
    }


    //---------------------------------------
    // Orders
    //---------------------------------------
    public function orders()
    {
        $data = [
            'page' => 'admin',
            'template' => 'template5',
            'awesomeFont' => true,
            'tab' => 'orders'
        ];

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $orders = $this->model->getOrders();

            if(!empty($orders) && count($orders) > 0)
            {
                $data['orders'] = $orders;
            } 
            else
            {
                $data['orders'] = null;
            }

            $this->view('pages/admin', $data);
        }
        else
        {
            $this->redirect(URLROOT."Member/login");
        }
    }

    //---------------------------------------
    // (Ajax) Confirm delivery complete
    //---------------------------------------
    public function ajaxCompleteDelivery()
    {

        // Check & Set Login Session
        session_start();
        $this->getSessionVar($data);

        if($data['isLoggedin'])
        {
            $orderid = -1;

            //---------------------------------------
            // Prevent Injection Attact Step1 : Strip slashes
            //---------------------------------------
            preventInjection_Stripslashes();
            preventInjection_TrimStrip();

            //---------------------------------------
            // Validation
            //---------------------------------------
            if(isset($_POST['orderid']) && !empty($_POST['orderid']) && is_numeric($_POST['orderid']))
            {
                $orderid = $_POST['orderid'];
            }
            else
            {
                echo '[{"success":false,"error":"Wrong Parameter"}]';
                exit;
            }

            // Complete the delivery
            $result = $this->model->updateStatus($orderid, 2);

            if($result == true)
            {
                echo '[{"success":true}]';
            }
            else
            {
                echo '[{"success":false, "error":"DB Query Error"}]';
            }
        }
        else
        {
            echo '[{"success":false, "error":"Are you hacking my site? or Please, logout and login, and then try it again."}]';
        }
    }
}

?>