<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Home extends Controller {

        public function __construct() {
            $this->modelFood = $this->model('Food');
            $this->modelTestimonial = $this->model('Testimonial');
        }

        //-------------------------
        // Show Home page
        //-------------------------
        public function index()
        {
            $data = [
                'page' => 'home',
                'template' => 'template1',
                'awesomeFont' => true
            ];

            // Check & Set Login Session
            session_start();
            $this->getSessionVar($data);

            // Favorite Food
            $favorites = $data['favorites'] = $this->modelFood->getFavoriteFoods(4);

            if(!isset($favorites) && count($favorites) <= 0)
            {
                $data['favorites'] = null;
            }

            // Testimonial
            $testimonial = $data['testimonial'] = $this->modelTestimonial->getTestimonial();

            if(!isset($testimonial) && count($testimonial) != 1)
            {
                $data['testimonial'] = null;
            }
            
            $this->view('pages/home', $data);
        }
    }

?>