<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Delivery extends Controller {

        public function __construct() {
            $this->model = $this->model('Food');
        }

        //-------------------------
        // Show Delivery page
        //-------------------------
        public function index()
        {

            $data = [
                'page' => 'delivery',
                'template' => 'template3',
                'awesomeFont' => true,
                'bootstrapCSS' => true
            ];

            // Check & Set Login Session
            session_start();
            $this->getSessionVar($data);


            // Recommend Foods
            $recommedFoodData = $data['recommend'] = $this->model->getRecommandFoods(2);
            
            if(!isset($recommedFoodData) && count($recommedFoodData) != 2)
            {
                $data['recommend'] = null;
            }


            // Foods to order
            $foodData = $data['foods'] = $this->model->getFoods();

            if(!isset($foodData) && count($foodData) <= 0)
            {
                $data['foods'] = null;
            }

            $this->view('pages/delivery', $data);
        }

        //-------------------------
        // Insert order data into DB
        //-------------------------
        public function ajaxOrder()
        {

            // Check & Set Login Session
            session_start();
            $this->getSessionVar($data);

            if($data['isLoggedin'])
            {
                $foodid = -1;

                //---------------------------------------
                // Prevent Injection Attact Step1 : Strip slashes
                //---------------------------------------
                preventInjection_Stripslashes();
                preventInjection_TrimStrip();

                //---------------------------------------
                // Validation
                //---------------------------------------
                if(isset($_POST['foodid']) && !empty($_POST['foodid']) && is_numeric($_POST['foodid']))
                {
                    $foodid = $_POST['foodid'];
                }
                else
                {
                    echo '[{"success":false,"error":"Wrong Parameter"}]';
                    exit;
                }

                // Order
                $result = $this->model->order($foodid, $_SESSION['userid']);
                if($result == true)
                {
                    echo '[{"success":true}]';
                }
                else
                {
                    echo '[{"success":false, "error":"DB Query Error"}]';
                }
            }
            else
            {
                echo '[{"success":false, "error":"Please, login and try to place orders."}]';
            }
        }
    }

?>