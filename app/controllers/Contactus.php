<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Contactus extends Controller {

        public function __construct() {
            $this->model = $this->model('Contactus');
            $this->modelFood = $this->model('Food');
            $this->modelTestimonial = $this->model('Testimonial');
        }

        //-------------------------
        // Show Contactus page
        //-------------------------
        public function index()
        {
            $data = [
                'page' => 'contactus',
                'template' => 'template4',
                'awesomeFont' => true
            ];

            // Check & Set Login Session
            session_start();
            $this->getSessionVar($data);
            
            // Favorite Food
            $favorites = $data['favorites'] = $this->modelFood->getFavoriteFoods(6);

            if(!isset($favorites) && count($favorites) <= 0)
            {
                $data['favorites'] = null;
            }

            // Testimonial
            $testimonial = $data['testimonial'] = $this->modelTestimonial->getTestimonial();

            if(!isset($testimonial) && count($testimonial) != 1)
            {
                $data['testimonial'] = null;
            }

            $this->view('pages/contactus', $data);
        }

        //-------------------------
        // Insert contacts info to DB
        //-------------------------
        public function ajax_setContactus()
        {
            $isValid = true;
            $isValidEmail = true;
            $isValidPhone = true;

            //---------------------------------------
            // Prevent Injection Attact Step1 : Strip slashes
            //---------------------------------------
            preventInjection_Stripslashes();
            preventInjection_TrimStrip();

            //---------------------------------------
            // Validation
            //---------------------------------------
            (isset($_POST['name']) && !empty($_POST['name'])) ? $name = $_POST['name'] : $isValid = false;
            (isset($_POST['email']) && !empty($_POST['email'])) ? $email = $_POST['email'] : $isValidEmail = false;
            (isset($_POST['phone']) && !empty($_POST['phone'])) ? $phone = $_POST['phone'] : $isValidPhone = false;
            (isset($_POST['purpose']) && !empty($_POST['purpose'])) ? $purpose = $_POST['purpose'] : $isValid = false;
            (isset($_POST['message']) && !empty($_POST['message'])) ? $message = $_POST['message'] : $isValid = false;

            if($isValid == false || ($isValidEmail | $isValidPhone) == false)
            {
                echo '[{"success":false,"error":"Wrong Parameter'.$isValidPhone.'"}]';
                exit;
            }

            if($isValidEmail == false){ $email = null; }
            else if($isValidPhone == false){ $phone = null; }

            // Set a Contactus
            $result = $this->model->setContactus($name, $email, $phone, $purpose, $message);

            //---------------------------------------
            // Email & Password Validation
            //---------------------------------------
            if (filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                // -------------------------------------
                // Send an email to a person posted
                // -------------------------------------
                $a = mail($email, 'Thank you!', "Thank you very much for contacting us.");    
            }
            
            // Send an email to admin
            $content = 'Name : '.$name;
            $content .= 'Email : '.$email;
            $content .= 'Phone : '.$phone;
            $content .= 'Purpose : '.$purpose;
            $content .= 'Message : '.$message;
            $b = mail(ADMIN_EMAIL, 'Contact us', $content);


            if($result == true)
            {
                echo '[{"success":true}]';
            }
            else
            {
                echo '[{"success":false, "error":"DB Query Error"}]';
            }
        }

    }

?>