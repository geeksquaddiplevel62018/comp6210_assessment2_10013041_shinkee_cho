<?php

    class Controller {

        public function model($model) {
            if(file_exists('../app/models/' . $model . '.php')) {
                require_once '../app/models/' . $model . '.php';
                $modelClass = '\\Model\\'.$model; // Namespace is Model
                return new $modelClass;
            } else {
                die('Model does not exists');
            }
        }

        public function view($view, $data = []) {
            if(file_exists('../app/views/' . $view . '.php')) {
                require_once '../app/views/' . $view . '.php';
            } else {
                die('View does not exists');
            }
        }

        public function getSessionVar(&$data)
        {            
            $data['isLoggedin'] = false;

            if(isset($_SESSION['email']))
            {
                $data['isLoggedin'] = true;
                $data['userid'] = $_SESSION['userid'];
                $data['email'] = $_SESSION['email'];
                $data['fname'] = $_SESSION['fname'];
                $data['level'] = $_SESSION['level'];
            }
        }

        public function setSessionVar($memberInfo)
        {
            $_SESSION['userid'] = $memberInfo['id'];
            $_SESSION['email'] = $memberInfo['email'];
            $_SESSION['fname'] = $memberInfo['fname'];
            $_SESSION['level'] = $memberInfo['level'];
        }

        public function redirect($url = URLROOT)
        {
            $data['redirectUrl'] = $url;
            $this->view('pages/redirect', $data);
        }
    }

?>