<?php

namespace Model;

    class FAQ {

        private $db;

        public function __construct()
        {
            $dbClass = '\Database';         // Using '\' because of namespace
            $this->db = new $dbClass;
        }

        public function getFAQ()
        {
            // To display from oldest to newest.
            $this->db->query('SELECT * FROM faq ORDER BY id ASC');
            return $this->db->resultSet();
        }
    }

?>