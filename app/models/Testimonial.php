<?php

namespace Model;

    class Testimonial {

        private $db;

        public function __construct() {
            $dbClass = '\Database';
            $this->db = new $dbClass;
        }


        public function getTestimonial($rows = 1)
        {
            // Use for only small tables.(RAND())
            $this->db->query('SELECT * FROM testimonial ORDER BY RAND() LIMIT :rows');
            $this->db->bind(":rows", $rows);
            
            return $this->db->resultSet();
        }

    }

?>