<?php

namespace Model;

    class Food {

        private $db;

        public function __construct()
        {
            $dbClass = '\Database';         // Using '\' because of namespace
            $this->db = new $dbClass;
        }

        public function getFoods($rows = 8)
        {
            $this->db->query('SELECT * FROM food LIMIT :rows');
            $this->db->bind(":rows", $rows);

            return $this->db->resultSet();
        }

        public function getRecommandFoods($rows = 2)
        {
            // Use for only small tables.(RAND())
            $this->db->query('SELECT * FROM food WHERE orders > 300 ORDER BY RAND() LIMIT :rows');
            $this->db->bind(":rows", $rows);
            
            return $this->db->resultSet();
        }

        public function getFavoriteFoods($rows = 5)
        {
            $this->db->query('SELECT * FROM food WHERE likes ORDER BY likes DESC LIMIT :rows');
            $this->db->bind(":rows", $rows);
            
            return $this->db->resultSet();
        }

        public function order($foodid, $userid)
        {
            $this->db->query('INSERT INTO  orders (food_id, user_id) VALUES (:fi, :ui)');

            $this->db->bind(':fi', $foodid);
            $this->db->bind(':ui', $userid);

            if($this->db->execute())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }

?>