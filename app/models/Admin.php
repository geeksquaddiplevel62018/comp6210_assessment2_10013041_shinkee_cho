<?php

namespace Model;

    class Admin {

        private $db;

        public function __construct()
        {
            $dbClass = '\Database';         // Using '\' because of namespace
            $this->db = new $dbClass;
        }

        public function getMyInfo($email)
        {
            $this->db->query('SELECT * FROM member WHERE email = :email');
            $this->db->bind(":email", $email);
            
            return $this->db->resultSet();
        }

        public function getContactus()
        {

            // To display from oldest to newest.
            $this->db->query("SELECT * FROM contactus ORDER BY completed, id ASC");
            return $this->db->resultSet();
        }

        public function getFeedback()
        {
            // To display from oldest to newest.
            $this->db->query('SELECT * FROM feedback ORDER BY id ASC');
            return $this->db->resultSet();
        }

        public function getFAQ()
        {
            // To display from oldest to newest.
            $this->db->query('SELECT * FROM faq ORDER BY id ASC');
            return $this->db->resultSet();
        }

        public function getMember()
        {
            // To display from oldest to newest.
            $this->db->query('SELECT * FROM member ORDER BY id ASC');
            return $this->db->resultSet();
        }

        public function getOrders()
        {
            // To display from oldest to newest.
            $this->db->query('SELECT orders.id, member.id as user_id, food.id as food_id, fname, food.name as food_name, img_path, status, orders.created_date FROM orders, food, member WHERE food_id=food.id AND user_id=member.id ORDER BY status, id ASC');
            return $this->db->resultSet();
        }

        // status
        // 0 : Processing
        // 1 : Canceled
        // 2 : Completed
        // 3 : Removed
        public function updateStatus($id, $status)
        {
            $this->db->query('UPDATE orders SET status = :st WHERE id = :id');

            $this->db->bind(':st', $status);
            $this->db->bind(':id', $id);

            if($this->db->execute())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

?>