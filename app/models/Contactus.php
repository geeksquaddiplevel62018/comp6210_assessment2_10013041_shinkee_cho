<?php

namespace Model;

    class Contactus {

        private $db;

        public function __construct()
        {
            $dbClass = '\Database';         // Using '\' because of namespace
            $this->db = new $dbClass;
        }

        public function setContactus($name, $email, $phone, $purpose, $message)
        {
            $this->db->query('INSERT INTO  contactus (name, email, phone, purpose, message) VALUES (:nm, :em, :ph, :pp, :ms)');

            $this->db->bind(':nm', $name);
            $this->db->bind(':em', $email);
            $this->db->bind(':ph', $phone);
            $this->db->bind(':pp', $purpose);
            $this->db->bind(':ms', $message);

            if($this->db->execute())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }

?>