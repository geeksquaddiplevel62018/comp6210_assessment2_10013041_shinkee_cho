<?php

namespace Model;

    class Member {

        private $db;

        public function __construct()
        {
            $dbClass = '\Database';         // Using '\' because of namespace
            $this->db = new $dbClass;
        }

        public function getMember($email)
        {
            $this->db->query('SELECT * FROM member WHERE email = :email');
            $this->db->bind(":email", $email);
            
            return $this->db->resultSet();
        }

        public function getMemberJson($email)
        {
            $this->db->query('SELECT * FROM member WHERE email = :email');
            $this->db->bind(":email", $email);
            
            return $this->db->showJSON();
        }

        public function addMember($email, $pw, $fname, $lname, $level = 1)
        {
            
            $this->db->query('INSERT INTO  member (email, password, fname, lname, level) VALUES (:em, :pw, :fn, :ln, :le)');

            $this->db->bind(':em', $email);
            $this->db->bind(':pw', $pw);
            $this->db->bind(':fn', $fname);
            $this->db->bind(':ln', $lname);
            $this->db->bind(':le', $level);

            if($this->db->execute(true))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        // public function getMyInfo($email)
        // {
        //     return $this->getMember($email);
        // }

        // public function changePassword()
        // {
            
        // }

        public function getTermsConditions()
        {
            $this->db->query('SELECT * FROM content WHERE name = :terms ORDER BY nth ASC');
            $this->db->bind(":terms", 'terms');

            return $this->db->resultSet();
        }
    }

?>