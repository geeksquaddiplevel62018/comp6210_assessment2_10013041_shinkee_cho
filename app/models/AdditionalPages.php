<?php

namespace Model;

    class AdditionalPages {

        private $db;

        public function __construct()
        {
            $dbClass = '\Database';         // Using '\' because of namespace
            $this->db = new $dbClass;
        }

        public function getAboutus()
        {
            $this->db->query('SELECT * FROM content WHERE inPage = :aboutus ORDER BY name, nth ASC');
            $this->db->bind(":aboutus", 'aboutus');

            return $this->db->resultSet();
        }
    }

?>