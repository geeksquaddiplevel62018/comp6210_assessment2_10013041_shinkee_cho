<?php

namespace Model;

    class Feedback {

        private $db;

        public function __construct()
        {
            $dbClass = '\Database';         // Using '\' because of namespace
            $this->db = new $dbClass;
        }

        public function setFeedback($satisfaction, $improvement)
        {

            $this->db->query('INSERT INTO  feedback (satisfaction, improvement) VALUES (:sf, :im)');

            $this->db->bind(':sf', $satisfaction);
            $this->db->bind(':im', $improvement);

            if($this->db->execute())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function getFeedback($rows = 30)
        {
            $this->db->query('SELECT * FROM feedback LIMIT :rows');
            $this->db->bind(":rows", $rows);
            
            return $this->db->resultSet();
        }

    }

?>