<?php

namespace Model;

    class MyPage {

        private $db;

        public function __construct()
        {
            $dbClass = '\Database';         // Using '\' because of namespace
            $this->db = new $dbClass;
        }

        public function getMyInfo($email)
        {
            $this->db->query('SELECT * FROM member WHERE email = :email');
            $this->db->bind(":email", $email);
            
            return $this->db->resultSet();
        }

        public function changePassword()
        {
            
        }

        public function getOrders($userID)
        {
            // To display from oldest to newest.
            $this->db->query('SELECT orders.id, food.name as food_name, img_path, status, orders.created_date FROM orders, food, member WHERE food_id=food.id AND user_id=:ui AND member.id=:ui AND orders.status<>3 ORDER BY status, id ASC');
            
            $this->db->bind(":ui", $userID);
            return $this->db->resultSet();
        }

        // status
        // 0 : Processing
        // 1 : Canceled
        // 2 : Completed
        // 3 : Removed
        public function updateStatus($id, $status)
        {
            $this->db->query('UPDATE orders SET status = :st WHERE id = :id');
            
            $this->db->bind(':st', $status);
            $this->db->bind(':id', $id);

            if($this->db->execute())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

?>