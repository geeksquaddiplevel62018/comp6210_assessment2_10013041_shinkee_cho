            <!-- Contact us -->
            <div id="contactus" class="contactus tabContent inline">
                <ul>
                    <li><input type='checkbox' name="select" /></li>
                    <li>ID</li>
                    <li>Name</li>
                    <li>Email</li>
                    <li>Phone</li>
                    <li>Purpose</li>
                    <li>Message</li>
                    <li>V</li>
                    <li>Date</li>
                </ul>
<?php
$contactus = $data['contactus'];
if($contactus != null):
    $cnt = count($contactus);
    for($i=0;$i<$cnt;$i++):
        $curContactus = $contactus[$i];
        $date = date_create($curContactus['created_date']);

?>
                <ul>
                    <li><input type='checkbox' name="select" /></li>
                    <li><?php echo $curContactus['id']; ?></li>
                    <li><?php echo $curContactus['name']; ?></li>
                    <li><?php echo $curContactus['email']; ?></li>
                    <li><?php echo $curContactus['phone']; ?></li>
                    <li><?php echo $curContactus['purpose']; ?></li>
                    <li><?php echo $curContactus['message']; ?></li>
                    <li><?php echo ($curContactus['completed'] == 0) ? '' : 'V'; ?></li>
                    <li><?php echo date_format($date, 'd.m.y'); ?></li>
                </ul>
<?php
    endfor;
endif;
?>
            </div>
            