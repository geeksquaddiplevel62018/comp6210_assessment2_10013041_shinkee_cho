<?php $layer = ($data['page'] == 'delivery') ? 'relative' : ''; ?>
        <div id='layerJ' class='layer <?php echo  $layer; ?>'>
            <div class='bg'>
                <img src='/imgs/food/bottom.png'>
            </div>

            <div class='gMap'>
                <iframe id='map' width='190' height='160' frameborder='0' style='border:0' src='https://www.google.com/maps/embed/v1/place?q=Toiohomai%20Bongard%20in%20tauranga&key=AIzaSyDkBxpd8i_6qcACWarvZb6K8VGLrztYGbA&language=en&zoom=14' allowfullscreen></iframe>
            </div>

            <div class='contactus'>
                <h1>Contact us</h1>
                <p><img src='/imgs/phone.png'><span>021 0270 1023</span></p>
                <p><img src='/imgs/email.png'><span>service@havethere.com</span></p>
                <p><img src='/imgs/address.png'><span>162 Love Street, Heaven,</p>
                <p class='newline'><span>Tauranga</span></p>
            </div>
        </div>