<?php
    $favorites = $data['favorites'];
    foreach($favorites as $key => $food):
?>
                <tr>
                <td><img src="<?php echo $food['img_path']?>"></td>
                <td><?php echo $food['name']?></td>
                <td><?php echo $food['price']?></td>
                <td><i class="fa heart rating<?php echo round($food['rating_level']); ?>"></i><br><?php echo $food['rating_level']?>(<?php echo $food['rating_cnt']?>)</td>
                <td><?php echo $food['orders']?></td>
                </tr>
<?php endforeach; ?>