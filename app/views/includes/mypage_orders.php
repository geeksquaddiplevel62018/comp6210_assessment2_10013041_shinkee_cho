            <!-- Order History -->
            <div id="orders" class="orders tabContent inline">
                <ul>
                    <li><input type='checkbox' name="select" /></li>
                    <li>ID</li>
                    <li>Food Name</li>
                    <li>Image</li>
                    <li>Status</li>
                    <li>Created Date & Time</li>
                    <li>Action</li>
                </ul>
<?php
if($data['orders'] != null):
    $orders = $data['orders'];

    $cnt = count($orders);
    for($i=0;$i<$cnt;$i++):
        $curOrders = $orders[$i];

        switch($curOrders['status'])
        {
            case 0:
                $strStatus = 'processing';
                $actionBtnName = 'Cancel';
                break;
            case 1:
                $strStatus = 'canceled'; 
                $actionBtnName = 'Remove';
                break;
            
            case 2:
                $strStatus = 'completed';
                $actionBtnName = 'Remove';
                break;

            case 3: // Removed one
                break;
        } 
?>
                <ul class="<?php echo $strStatus; ?>">
                    <li><input type='checkbox' name="select" /></li>
                    <li><?php echo $curOrders['id']; ?></li>
                    <li><?php echo $curOrders['food_name']; ?></li>
                    <li><img src="<?php echo $curOrders['img_path']; ?>"></li>
                    <li><span class="status <?php echo $strStatus; ?>"><?php echo $strStatus; ?></span></li>
                    <li><?php echo $curOrders['created_date'] ?></li>
                    <li><button class="btn<?php echo $actionBtnName; ?>" orderid="<?php echo $curOrders['id']; ?>"><?php echo $actionBtnName; ?></button></li>
                </ul>
<?php
    endfor;
endif;
?>
            </div>
            
            <script>
                //---------------------------------------
                // (Ajax) Cancel
                //---------------------------------------
                $(".btnCancel").click(function(){
                    var curjBtn = $(this);

                    var action = '/MyPage/ajaxCancelOrder/';
                    var form_data = {
                        orderid: $(this).attr('orderid')
                    };
                    
                    $.ajax({
                        type: "POST",
                        url: action,
                        data: form_data,
                        success: function(response)
                        {
                            console.log(response);
                            var responseJson = jQuery.parseJSON(response)[0];
                            
                            if(responseJson.success == true)
                            {
                                // Remove button
                                curjBtn.fadeOut(300, 'easeOutCubic',function(){
                                    $(this).parents('ul').removeClass().addClass('canceled');
                                    $(this).parents('ul').find('.status').html('Canceled');
                                    console.log($(this).parents('ul'));
                                    console.log($(this).parents('ul').children('.status'));

                                    
                                    var btnjRemove = $("<button class='btnRemove' orderid='"+form_data.orderid+"'>Remove</button>");
                                    $(this).parent().append(btnjRemove);
                                    btnjRemove.click(removeOrder);

                                });
                            }
                            // Failed
                            else
                            {
                                MessageBox.Show("Fail", responseJson['error']);
                            }
                        }
                    });
                    return false;
                });

                //---------------------------------------
                // (Ajax) Remove
                //---------------------------------------
                $(".btnRemove").click(removeOrder);
                function removeOrder(){
                    var curjBtn = $(this);

                    var action = '/MyPage/ajaxRemoveOrder/';
                    var form_data = {
                        orderid: $(this).attr('orderid')
                    };
                    
                    $.ajax({
                        type: "POST",
                        url: action,
                        data: form_data,
                        success: function(response)
                        {
                            console.log(response);
                            var responseJson = jQuery.parseJSON(response)[0];
                            
                            if(responseJson.success == true)
                            {
                                // Remove item
                                curjBtn.parents('ul').fadeOut(300, 'easeOutSine',function(){
                                    $(this).remove();
                                });
                            }
                            // Failed
                            else
                            {
                                MessageBox.Show("Fail", responseJson['error']);
                            }
                        }
                    });
                    return false;
                }
            </script>