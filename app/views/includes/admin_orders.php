            <!-- Order History -->
            <div id="orders" class="orders tabContent inline">
                <ul>
                    <li><input type='checkbox' name="select" /></li>
                    <li>ID</li>
                    <li>User ID</li>
                    <li>User Name</li>
                    <li>Food ID</li>
                    <li>Food Name</li>
                    <li>Image</li>
                    <li>Status</li>
                    <li>Created Date & Time</li>
                </ul>   
<?php
if($data['orders'] != null):
    $orders = $data['orders'];

    $cnt = count($orders);
    for($i=0;$i<$cnt;$i++):
        $curOrders = $orders[$i];
?>
                <ul>
                    <li><input type='checkbox' name="select" /></li>
                    <li><?php echo $curOrders['id']; ?></li>
                    <li><?php echo $curOrders['user_id']; ?></li>
                    <li><?php echo $curOrders['fname']; ?></li>
                    <li><?php echo $curOrders['food_id']; ?></li>
                    <li><?php echo $curOrders['food_name']; ?></li>
                    <li><img src="<?php echo $curOrders['img_path']; ?>"></li>
                    <li>
                        <?php if($curOrders['status'] == 0): ?>
                            <button class="btnComplete" orderid="<?php echo $curOrders['id']; ?>">Complete</button>
                        <?php elseif($curOrders['status'] == 1): ?>
                            <span class="canceled">Canceled</span>
                        <?php elseif($curOrders['status'] == 2): ?>
                            <span class="completed">Completed</span>
                        <?php elseif($curOrders['status'] == 3): ?>
                            <span class="removed">Removed</span>
                        <?php else: ?>
                            <span class="error">Error</span>
                        <?php endif;?>
                    </li>
                    <li><?php echo $curOrders['created_date'] ?></li>
                </ul>
<?php
    endfor;
endif;
?>
            </div>

            <script>
                var curCompletejBtn;
                $(".btnComplete").click(function(){
                    curCompletejBtn = $(this);

                    var action = '/Admin/ajaxCompleteDelivery/';
                    var form_data = {
                        orderid: $(this).attr('orderid')
                    };
                    
                    $.ajax({
                        type: "POST",
                        url: action,
                        data: form_data,
                        success: function(response)
                        {
                            console.log(response);
                            var responseJson = jQuery.parseJSON(response)[0];
                            
                            if(responseJson.success == true)
                            {
                                // Remove button
                                curCompletejBtn.fadeOut(300, 'easeOutCubic',function(){
                                    console.log($(this));
                                    $(this).parent().html('Completed');
                                });
                            }
                            // Failed
                            else
                            {
                                MessageBox.Show("Fail", responseJson['error']);
                            }
                        }
                    });
                    return false;
                });
            </script>