<div class="s_top">
    <ul>
        <li><a href="/"><img class="logo" src="/imgs/logo.png"></a></li>
        <li class="companyName"><a href="/"><?php echo SITENAME; ?></a></li>
    </ul>

    <ul class="s_menu <?php echo $data['page']; ?>">
        <li class="menuTab menu"><a class="icon">&#9776;</a></li>
        <li class="about menu"><a href="About/">About</a></li>
        <li class="delivery menu"><a href="Delivery/">Delivery</a></li>
        <li class="faq menu"><a href="FAQ/">FAQ</a></li>
        <li class="feedback menu"><a href="Feedback/">Feedback</a></li>
        <li class="contactus menu"><a href="Contactus/">Contact us</a></li>
        <li class="gap menu"></li>   

<?PHP if($data["isLoggedin"]): ?>
    <?PHP if($data["level"] == 10): ?>
        <li class="adminpage menu member"><a href="Admin/"><?php echo $data['fname'];?>(ADMIN)</a></li>
    <?php else:?>
        <li class="mypage menu member"><a href="MyPage/"><?php echo $data["fname"]; ?></a></li>
    <?php endif;?>
        <li class="logout menu"><a href="Member/logout">Logout</a></li>
<?php else:?>
        <li class="login menu member"><a href="Member/login">Login</a></li>
        <li class="signup menu"><a href="Member/signup">Signup</a></li>
<?php endif;?>
    </ul>
</div>