            <!-- Member -->
            <div id="member" class="member tabContent inline">
                <ul>
                    <li><input type='checkbox' name="select" /></li>
                    <li>ID</li>
                    <li>First Name</li>
                    <li>Last Name</li>
                    <li>Email</li>
                    <li>Password</li>
                    <li>Group</li>
                    <li>Created Date & Time</li>
                </ul>                
<?php
if($data['member'] != null):
    $member = $data['member'];
    $cnt = count($member);
    for($i=0;$i<$cnt;$i++):
        $curMember = $member[$i];

        $group = "Guest";
        switch($curMember['level'])
        {
            case '1' : $group = "Member"; break;
            case '5' : $group = "VIP"; break;
            case '10' : $group = "Admin"; break;
            default : break;
        }
?>
                <ul>
                    <li><input type='checkbox' name="select" /></li>
                    <li><?php echo $curMember['id']; ?></li>
                    <li><?php echo $curMember['fname']; ?></li>
                    <li><?php echo $curMember['lname']; ?></li>
                    <li><?php echo $curMember['email']; ?></li>

                    <li><?php echo $curMember['password']; ?></li>

                    <li><?php echo $group; ?></li>
                    <li><?php echo $curMember['created_date'] ?></li>
                </ul>
<?php
    endfor;
endif;
?>
            </div>