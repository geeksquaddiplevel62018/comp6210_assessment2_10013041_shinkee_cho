<head>
    <title><?php echo SITENAME; ?></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo URLROOT; ?>" target="_self">

    <link rel="shortcut icon" href="imgs/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/imgs/favicon.ico" type="image/x-icon">
     
<?php if(isset($data['bootstrapCSS'])): ?>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php endif; ?>   
    <link rel="stylesheet" type="text/css" media="screen" href="/css/common.css" />
<?php if($data['template'] != 'template5'): ?>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/layers.css" />
<?php endif; ?>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/<?php echo $data['template']; ?>.css" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
<?php if(isset($data['awesomeFont'])): ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php endif; ?>

    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/easing.js"></script>
<?php if(isset($data['bootstrapJS'])): ?>
    <!-- Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>
<?php endif; ?>
<?php if($data['template'] != 'template5'): ?>
    <script src="/js/layers.js"></script>
<?php endif; ?>
    <script src="/js/common.js"></script>
</head>