<?php
    if($data['recommend'] != null):
        $recommend1 = $data['recommend'][0];
        $recommend2 = $data['recommend'][1];
?>
        <!-- Recommendation -->
        <div id="layerB" class="layer">
            <ul class="foodInfo">
                <li class="name"><h1><?php echo $recommend1['name']; ?></h1></li>
                <li class="rating"><i class="fa heart rating<?php echo round($recommend1['rating_level']); ?>"></i> <?php echo $recommend1['rating_level']; ?>(<?php echo $recommend1['rating_cnt']; ?>)</li>
                <li class="text">Highly recommended</li>
                <li class="views">
                    <ul>
                        <li class="number"><?php echo $recommend1['views']; ?></li>
                        <li class="text">views</li>
                    </ul>
                    <span class="slash">/</span>
                    <ul>
                        <li class="number"><?php echo $recommend1['likes']; ?></li>
                        <li class="text">likes</li>
                    </ul>
                </li>
                <li class="order"><button foodid="<?php echo $recommend1['id']; ?>" class="btnOrder">Order</button></li>
            </ul>
            <img class="bgFood" src="<?php echo $recommend1['img_path']; ?>">
        </div>

        <!-- Recommendation -->
        <div id="layerC" class="layer">
            <ul class="foodInfo">
                <li class="name"><h1><?php echo $recommend2['name']; ?></h1></li>
                <li class="rating"><i class="fa heart rating<?php echo round($recommend2['rating_level']); ?>"></i> <?php echo $recommend2['rating_level']; ?>(<?php echo $recommend2['rating_cnt']; ?>)</li>
                <li class="text">Highly recommended</li>
                <li class="views">
                    <ul>
                        <li class="number"><?php echo $recommend2['views']; ?></li>
                        <li class="text">views</li>
                    </ul>
                    <span class="slash">/</span>
                    <ul>
                        <li class="number"><?php echo $recommend2['likes']; ?></li>
                        <li class="text">likes</li>
                    </ul>
                </li>
                <li class="order"><button foodid="<?php echo $recommend2['id']; ?>" class="btnOrder">Order</button></li>
            </ul>
            <img class="bgFood" src="<?php echo $recommend2['img_path']; ?>">
        </div>
<?php endif; ?>