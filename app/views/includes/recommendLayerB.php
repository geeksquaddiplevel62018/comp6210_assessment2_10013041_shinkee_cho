<?php 
    $favorites = $data['favorites'];
    $cnt = count($favorites);
    $random = mt_rand(0, $cnt-1);
    $recommend = $favorites[$random];
?>
        <!-- Recommanded Food -->
        <div id="layerB" class="layer">
            <ul class="foodInfo">
                <li class="name"><h1><?php echo $recommend['name']; ?></h1></li>
                <li class="rating"><i class="fa heart rating<?php echo round($recommend['rating_level']); ?>"></i> <?php echo $recommend['rating_level']; ?>(<?php echo $recommend['rating_cnt']; ?>)</li>
                <li class="text">Highly recommended</li>
                <li class="views">
                    <ul>
                        <li class="number"><?php echo $recommend['views']; ?></li>
                        <li class="text">views</li>
                    </ul>
                    <span class="slash">/</span>
                    <ul>
                        <li class="number"><?php echo $recommend['likes']; ?></li>
                        <li class="text">likes</li>
                    </ul>
                </li>
                <li class="order"><button foodid="<?php echo $recommend['id']; ?>" class="btnOrder">Order</button></li>
            </ul>
            <img class="bgFood" src="<?php echo $recommend['img_path']; ?>">
        </div>  