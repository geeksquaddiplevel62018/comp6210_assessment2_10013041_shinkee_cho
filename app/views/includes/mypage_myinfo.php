<?php
    if($data['tab'] == 'myinfo'):
        
        $group = "Guest";

        switch($data['myinfo']['level'])
        {
            case '1' : $group = "Member"; break;
            case '5' : $group = "VIP Member"; break;
            case '10' : $group = "Admin"; break;
            default : break;
        }
?>
            <!-- My Info -->
            <div id="myInfo" class="myInfo tabContent">
                <ul>
                    <li>
                    <div class="title">First Name</div>
                    <div class="value"><?php echo $data['myinfo']['fname']; ?></div>
                    </li>

                    <li>
                    <div class="title">Last Name</div>
                    <div class="value"><?php echo $data['myinfo']['lname']; ?></div>
                    </li>

                    <li>
                    <div class="title">Email</div>
                    <div class="value"><?php echo $data['myinfo']['email']; ?></div>
                    </li>

                    <li>
                    <div class="title changePassword">Change Password</div>
                    <div class="value changePassword">
                        <input type='text' class="curPW" placeholder="Current Password" />
                        <input type='text' class="newPW" placeholder="New Password" />
                        <input type='text' class="confirmPW" placeholder="Confirm Password" />
                        <input type='submit' class="btnChangePW" value="Change Password"/>
                    </div>
                    </li>

                    <li>
                    <div class="title">Group</div>
                    <div class="value"><?php echo $group; ?></div>
                    </li>

                    <li>
                    <div class="title">Created Date & Time</div>
                    <div class="value"><?php echo $data['myinfo']['created_date'] ?></div>
                    </li>
                </ul>
            </div>
<?php endif; ?>