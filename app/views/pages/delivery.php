<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<div class="s_main">
    <div class="s_container">
        
        <!-- Social media icons -->
        <?php include(APPROOT . "/views/includes/socialMediaIcons.php"); ?>
        
        <!-- Recomend Layer B, C -->
        <?php include(APPROOT . "/views/includes/recommendLayerBC.php"); ?>

        <!-- Items to deliver -->
<?php
    if($data['foods'] != null):
        $foods = $data['foods'];
        $cnt = count($foods);
?>
        <div id="layerE" class="deliveryItemsContainer">
            <div class="container">
                <div class="row">

<?php
        for($i=0;$i<$cnt;$i++):
            $foodID = $foods[$i]['id'];
            $rating = $foods[$i]['rating_level'];
            $foodName = $foods[$i]['name'];
            $desc = $foods[$i]['f_desc'];
            $imgPath = $foods[$i]['img_path'];
?>
                    <div class="col-sm">
                        
<?php       if($i%4 != 3): ?>
                        <div class="item col">
<?php       else: ?>
                        <div class="item colLast min1px">
<?php       endif; ?>
                            <img class="bgFood" src="<?php echo $imgPath; ?>">
                            <div class="ratingBox"><i class="fa heart rating"></i><span><?php echo $rating; ?></span></div>
                            <ul class="foodInfo">
                                <li class="name"><h1><?php echo $foodName; ?></h1></li>
                                <li class="desc"><?php echo $desc; ?></li>
                                <li class="order"><button foodid="<?php echo $foodID; ?>" class="btnOrder">Order</button></li>
                            </ul>
                        </div>
                    </div>
<?php
        endfor;
    endif;
?>

                </div>  <!-- row end -->
            </div>

            <!-- Banner -->
            <div id="layerH" class="layer relative rightMargin15px">
                <img class="bg" src="/imgs/logo.png">
                <ul>
                    <li>Have</li>
                    <li>There!</li>
                </ul>
            </div>
            
            <!-- Bottom -->
            <?php include(APPROOT . "/views/includes/bottom.php"); ?>

        </div>
    </div>
</div>

<script>
    Layout.arrange("template3");
</script>

<script>

var curOrderedjBtn;

function init()
{
    $(".btnOrder").click(function(event) {

        curOrderedjBtn = $(this);

        //event.preventDefault();
		var action = 'Delivery/ajaxOrder/';
		var form_data = {
			foodid: $(this).attr('foodid')
		};
        
        $.ajax({
			type: "POST",
			url: action,
			data: form_data,
            success: function(response)
            {
                console.log(response);
                var responseJson = jQuery.parseJSON(response)[0];
				
                if(responseJson.success == true)
                {
                    droppingAnimation(responseJson, $("#formLogin"), curOrderedjBtn, $(".s_menu .menu.member"), function(){ });
                }
                // Login failed
                else
                {
                    MessageBox.Show("Order fail", responseJson.error);
				}
			}
		});
		return false;
    });
}
</script>

</body>
</html>