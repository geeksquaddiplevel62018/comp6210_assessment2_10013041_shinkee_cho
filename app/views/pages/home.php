<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<div class="s_main">
    <div class="s_container">

        <!-- Social media icons -->
        <?php include(APPROOT . "/views/includes/socialMediaIcons.php"); ?>

        <!-- Recommanded Food -->
        <?php include(APPROOT . "/views/includes/recommendLayerB.php"); ?>

        <!-- Ingredients -->
        <div id="layerC" class="layer">
            <img class="bg" src="/imgs/food/ingredients.png">
            <div class="filter"></div>
            <ul class="Ingredients">
                <li><h1>Ingredients</h1></li>
                <li>
                    <span class="left">200g</span>
                    <span class="right">fresh linguine</span>
                </li>
                <li>
                    <span class="left">2-3</span>
                    <span class="right">cups tomatoes</span>
                </li>
                <li>
                    <span class="left">1/4</span>
                    <span class="right">cup fresh herbs</span>
                </li>
                <li>
                    <span class="left">1-2</span>
                    <span class="right">garlic cloves</span>
                </li>
                <li>
                    <span class="left">4</span>
                    <span class="right">tablespoons oil</span>
                </li>
                <li>
                    <span class="left">1</span>
                    <span class="right">spoon vinegar</span>
                </li>
            </ul>
        </div>

        <!-- Weather ticker -->
        <div id="layerD" class="layer">
            <div id="metservice-widget"></div>
        </div>

        <!-- Testimonial -->
        <?php include(APPROOT . "/views/includes/testimonial.php"); ?>

        <!-- Favorite Foods -->
        <div id="layerF" class="layer">
            <h1>Foods to delivery</h1>
            <table>
                <tr>
                <th>Food</th>
                <th></th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Add</th>
                </tr>

                <!-- Favorites -->
                <?php include(APPROOT . "/views/includes/favorites.php"); ?>

            </table>

            <div class="btns">
                <button class="myCart">Go to MyCart</button>
                <button class="emptyList">Empty List</button>
            </div>
        </div>

        <!-- Selected food -->
        <div id="layerG" class="layer">
            <h1>Pizza Special</h1>
            <p>Price : $15.00</p>
            <div class="ratingBox"><i class="fa heart rating"></i><span>4.8</span></div>
            <img class="bg" src="/imgs/food/sipa.s-palvelut.png">
            <div class="caption">
                <button class="more">MORE</button>
                <span>More information.</span>
            </div>
        </div>
        
        <!-- Twitter feed -->
        <div id="layerH" class="layer">
        <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/search?q=%23italian%20food" data-widget-id="981739317683879936">Tweets about #italian food</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>

        <!-- Bottom -->
        <?php include(APPROOT . "/views/includes/bottom.php"); ?>

    </div>
</div>

<script>
    Layout.arrange("template1");
</script>

<!-- Weather ticker start -->
<script>
(function(d){
    var i = d.createElement("iframe");
    i.setAttribute("src", "https://services.metservice.com/weather-widget/widget?params=blue|large|portrait|days-3|modern&loc=tauranga&type=urban&domain=" + d.location.hostname);
    i.style.width = "300px";
    i.style.height = "239px";
    i.style.border = "0";
    i.setAttribute("allowtransparency", "true");
    i.setAttribute("id", "widget-iframe");
    d.getElementById("metservice-widget").appendChild(i);
})(document);
</script>
<style>
    #metservice-widget { margin:15px; background: #083156; }
</style>
<!-- Weather ticker end -->

<script>
function init()
{
    $(".btnOrder").click(function(event) {

        curOrderedjBtn = $(this);

        //event.preventDefault();
		var action = 'Delivery/ajaxOrder/';
		var form_data = {
			foodid: $(this).attr('foodid')
		};
        
        $.ajax({
			type: "POST",
			url: action,
			data: form_data,
            success: function(response)
            {
                console.log(response);
                var responseJson = jQuery.parseJSON(response)[0];
				
                if(responseJson.success == true)
                {
                    droppingAnimation(responseJson, $("#formLogin"), curOrderedjBtn, $(".s_menu .menu.member"), function(){ });
                }
                // Login failed
                else
                {
                    MessageBox.Show("Order fail", responseJson.error);
				}
			}
		});
		return false;
    });
}
</script>
</body>
</html>