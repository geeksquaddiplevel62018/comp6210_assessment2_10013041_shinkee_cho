<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<div class="s_main">
    <div class="s_container">

        <!-- Social media icons -->
        <?php include(APPROOT . "/views/includes/socialMediaIcons.php"); ?>

        <!-- Recommanded Food -->
        <?php include(APPROOT . "/views/includes/recommendLayerB.php"); ?>


        <!-- FAQ Form -->
        <div id="layerD" class="layer">
            <div class="faq">
                <div class="title">FAQ</div>  
                <div class="listBox">
                    <ul class="list">
                    
<?php 
    $output = "";
    foreach($data['faqList'] as $faq):
?>
                        <li>
                            <ul>
                                <button type="button" class="question btn btn-info" data-toggle="collapse" data-target="#answer<?php echo $faq['id']; ?>">
                                    <?php echo $faq['question']; ?>
                                </button>
                                <div id="answer<?php echo $faq['id']; ?>" class="answer collapse in">
                                    <?php echo $faq['answer']; ?>
                                </div>
                            </ul>
                        </li>

<?php endforeach; ?>

                    </ul>
                </div>
            </div>
        </div>

        <!-- Testimonial -->
        <?php include(APPROOT . "/views/includes/testimonial.php"); ?>
        
        <!-- Favorite Foods -->
        <div id="layerH" class="layer">
            <h1>Favorite Foods</h1>
            <table>
                <tr>
                <th>Food</th>
                <th></th>
                <th>Price</th>
                <th>Rating</th>
                <th>Order</th>
                </tr>
                
                <!-- Favorites -->
                <?php include(APPROOT . "/views/includes/favorites.php"); ?>

            </table>
        </div>
        
        <!-- Bottom -->
        <?php include(APPROOT . "/views/includes/bottom.php"); ?>

    </div>
</div>

<script>
    Layout.arrange("template4");
</script>

<script>
function init()
{
    $(".btnOrder").click(function(event) {

        curOrderedjBtn = $(this);

        //event.preventDefault();
		var action = 'Delivery/ajaxOrder/';
		var form_data = {
			foodid: $(this).attr('foodid')
		};
        
        $.ajax({
			type: "POST",
			url: action,
			data: form_data,
            success: function(response)
            {
                console.log(response);
                var responseJson = jQuery.parseJSON(response)[0];
				
                if(responseJson.success == true)
                {
                    droppingAnimation(responseJson, $("#formLogin"), curOrderedjBtn, $(".s_menu .menu.member"), function(){ });
                }
                // Login failed
                else
                {
                    MessageBox.Show("Order fail", responseJson.error);
				}
			}
		});
		return false;
    });
}
</script>


</body>
</html>