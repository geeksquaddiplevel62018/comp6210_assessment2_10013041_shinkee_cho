<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<!--
// Tap 3개, 피드백, 컨텍트 어스, 주문, 자기정보
// 삭제 기능
// 수정

-->
<div class="s_main">
    <div class="s_container admin">
        
        <!-- Tabs -->
        <div id='layerFull' class='layer layerFull <?php echo $data['tab']; ?>'>
            <ul class="tabContainer">
                <!-- My Info -->
                <li id="tabMyInfo" class="tabMyInfo tab">My Info</li>
                
                <!-- Contact us -->
                <li id="tabContactus" class="tabContactus tab">Contact us</li>
                
                <!-- Feedback -->
                <li id="tabFeedback" class="tabFeedback tab">Feedback</li>
                
                <!-- FAQ -->
                <li id="tabFAQ" class="tabFAQ tab">Faq</li>
                
                <!-- Member -->
                <li id="tabMember" class="tabMember tab">Member</li>
                
                <!-- Orders -->
                <li id="tabOrders" class="tabOrders tab">Orders</li>

            </ul>

<?php
    if($data[$data['tab']] == null)
    {
        echo "<script>MessageBox.Show('Notice', 'There are not any data.')</script>";
    }

    switch($data['tab']):
        
        case 'myinfo' :
            include(APPROOT . "/views/includes/admin_myinfo.php");
            break;

        case 'contactus':
            
            include(APPROOT . "/views/includes/admin_contactus.php");
            break;
        
        case 'feedback':
            include(APPROOT . "/views/includes/admin_feedback.php");
            break;

        case 'faq':
            include(APPROOT . "/views/includes/admin_faq.php");
            break;

        case 'member':
            include(APPROOT . "/views/includes/admin_member.php");
            break;

        case 'orders':
            include(APPROOT . "/views/includes/admin_orders.php");
            break;

        default:
            break;
    
    endswitch;
?>


<!--
/// 탭
// 캔택어스(삭제), 피드백(삭제), faq(추가, 삭제, 수정), 음식(추가, 삭제, 수정), 주문현황(삭제), 맴버(추가, 삭제, 수정)
// -->
        </div>
    </div>
</div>

<script>
    var curTab = "<?php echo $data['tab']; ?>";
    $("#tabMyInfo").click(function(){ moveTab('myinfo'); });
    $("#tabContactus").click(function(){ moveTab('contactus'); });
    $("#tabFeedback").click(function(){ moveTab('feedback'); });
    $("#tabFAQ").click(function(){ moveTab('faq'); });
    $("#tabMember").click(function(){ moveTab('member'); });
    $("#tabOrders").click(function(){ moveTab('orders'); });

    function moveTab(targetTab)
    {
        if(curTab != targetTab)
        {
            $(location).attr('href','Admin/'+targetTab);
        }
    }
</script>

<script type="text/javascript">

</script>

</body>
</html>