<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<div class="s_main">
    <div class="s_container">

        <!-- Social media icons -->
        <?php include(APPROOT . "/views/includes/socialMediaIcons.php"); ?>

        <!-- Recommanded Food -->
        <?php include(APPROOT . "/views/includes/recommendLayerB.php"); ?>   

        <!-- Contact Form -->
        <div id="layerD" class="layer">
            <form id="contactus" action="#">
                <div class="title">Contact us Form</div>
                <input id="inputName" type="text" name="name" placeholder="Name">
                <input id="inputEmail" type="email" name="email" placeholder="your@email.com">
                <input id="inputPhone" type="number" name="phone" placeholder="Phone">
                <select id="selector" type="selector" name="Purpose">
                    <option value="">Select Purpose</option>
                    <option value="claim">Claim</option>
                    <option value="query">Query</option>
                    <option value="other">Other</option>
                </select>
                <textarea id="txtMessage" class="message" type="text" row="5" placeholder="Message"></textarea>

                <input id="btnContactus" type="submit" value="Send it">
            </form>

            <div id="message" class="message hide">Thank you!</div>
        </div>

        <!-- Testimonial -->
        <?php include(APPROOT . "/views/includes/testimonial.php"); ?>
        
        <!-- Favorite Foods -->
        <div id="layerH" class="layer">
            <h1>Favorite Foods</h1>
            <table>
                <tr>
                <th>Food</th>
                <th></th>
                <th>Price</th>
                <th>Rating</th>
                <th>Order</th>
                </tr>

                <!-- Favorites -->
                <?php include(APPROOT . "/views/includes/favorites.php"); ?>
            </table>
        </div>

        <!-- Bottom -->
        <?php include(APPROOT . "/views/includes/bottom.php"); ?>

    </div>
</div>

<script>
    Layout.arrange("template4");
</script>

<script type="text/javascript">
function init()
{

    // Check Form input field
    $('input').keyup(function(){ checkForm($(this)); });
    $('textarea').keyup(function(){ checkForm($(this)); });
    $('select').on('change', function(){ checkForm($(this)); });

	$("#btnContactus").click(function() {

        event.preventDefault();

        if(checkFormAll($("#contactus")) == false)
        {
            MessageBox.Show('ERROR', 'Please, type in correct values.');
            return false;
        }

		var action = 'Contactus/ajax_setContactus/';
		var form_data = {
            name: $("#inputName").val(),
            email: $("#inputEmail").val(),
            phone: $("#inputPhone").val(),
            purpose: $("#selector option:selected").val(),
            message: $("#txtMessage").val()
		};
        
        $.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response) {
                console.log(response);

                var responseJson = jQuery.parseJSON(response)[0];
				
                if(responseJson.success == true) {
                    $("#contactus").fadeOut();
                    $("#message").fadeIn();
				}
				else {
					alert("[Error] " + responseJson.error);
				}
			}
		});
		return false;
    });
    
    $(".btnOrder").click(function(event) {

        curOrderedjBtn = $(this);

        //event.preventDefault();
        var action = 'Delivery/ajaxOrder/';
        var form_data = {
            foodid: $(this).attr('foodid')
        };

        $.ajax({
            type: "POST",
            url: action,
            data: form_data,
            success: function(response)
            {
                console.log(response);
                var responseJson = jQuery.parseJSON(response)[0];
                
                if(responseJson.success == true)
                {
                    droppingAnimation(responseJson, $("#formLogin"), curOrderedjBtn, $(".s_menu .menu.member"), function(){ });
                }
                // Login failed
                else
                {
                    MessageBox.Show("Order fail", responseJson.error);
                }
            }
        });
        return false;
    });
}

</script>

</body>
</html>