<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<div class="s_main">
    <div class="s_container mypage">
        
        <!-- Tabs -->
        <div id='layerFull' class='layer layerFull <?php echo $data['tab']; ?>'>
            <ul class="tabContainer">
                <!-- My Info -->
                <li id="tabMyInfo" class="tabMyInfo tab">My Info</li>
                <!-- Order History -->
                <li id="tabOrders" class="tabOrders tab">My Orders</li>
            </ul>
<?php
    if($data[$data['tab']] == null)
    {
        echo "<script>MessageBox.Show('Notice', 'There are not any data.')</script>";
    }

    switch($data['tab'])
    {
        case 'myinfo' :
            include(APPROOT . "/views/includes/mypage_myinfo.php");
            break;

        case 'orders':
            include(APPROOT . "/views/includes/mypage_orders.php");
            break;

        default:
            break;
    }
?>
        </div>
    </div>
</div>

<script>
    var curTab = "<?php echo $data['tab']; ?>";
    $("#tabMyInfo").click(function(){ moveTab('myinfoTab'); });
    $("#tabOrders").click(function(){ moveTab('ordersTab'); });

    function moveTab(targetTab)
    {
        if(curTab != targetTab)
        {
            $(location).attr('href','MyPage/'+targetTab);
        }
    }
</script>


<script type="text/javascript">


</script>

</body>
</html>