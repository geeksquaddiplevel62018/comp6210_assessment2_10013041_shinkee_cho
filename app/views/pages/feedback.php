<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<div class="s_main">
    <div class="s_container">
        
        <!-- Social media icons -->
        <?php include(APPROOT . "/views/includes/socialMediaIcons.php"); ?>

        <!-- Recommanded Food -->
        <?php include(APPROOT . "/views/includes/recommendLayerB.php"); ?>  

        <!-- Feedback Form -->
        <div id="layerD" class="layer">
            <form id="feedback" action="#">
                <div class="title">Feedback Form</div>
                
                <div class="question">Overall, how did you feel about the service you received today?</div>
                <div class="radio">
                    <input type="radio" id="satisfy5" name="satisfy" value="5" checked />
                    <label for="satisfy5">Very Satisfied</label><br>
                    <input type="radio" id="satisfy4" name="satisfy" value="4" />
                    <label for="satisfy4">Satisfied</label><br>
                    <input type="radio" id="satisfy3" name="satisfy" value="3" />
                    <label for="satisfy3">Neither Satisfied or dissatisfied</label><br>
                    <input type="radio" id="satisfy2" name="satisfy" value="2" />
                    <label for="satisfy2">Dissatisfied</label><br>
                    <input type="radio" id="satisfy1" name="satisfy" value="1" />
                    <label for="satisfy1">Very dissatisfied</label>
                </div>

                <div class="question">How could we improve this service?</div>
                
                <textarea id="txtMessage" type="text" class="improvement" row="5" placeholder="Message"></textarea>
                <div class="limit">(Limit is 1200 characters)</div><br>

                <div class="warning"><q class="q1"></q>Please don't include any personal or financial information,
                    for example your National Insurance or Credit card numbers.<q class="q2"></q></div>
                <input id="btnFeedback" type="submit" value="Send it">
            </form>

            <div id="message" class="message hide">Thank you!</div>
        </div>
        
        <!-- Testimonial -->
        <?php include(APPROOT . "/views/includes/testimonial.php"); ?>
        
        <!-- Favorite Foods -->
        <div id="layerH" class="layer">
            <h1>Favorite Foods</h1>
            <table>
                <tr>
                <th>Food</th>
                <th></th>
                <th>Price</th>
                <th>Rating</th>
                <th>Order</th>
                </tr>
                
                <!-- Favorites -->
                <?php include(APPROOT . "/views/includes/favorites.php"); ?>

            </table>
        </div>

        <!-- Bottom -->
        <?php include(APPROOT . "/views/includes/bottom.php"); ?>

    </div>
</div>

<script>
    Layout.arrange("template4");
</script>

<script type="text/javascript">

function init()
{
    
    $('textarea').keyup(function(){ checkForm($(this)); });

	$("#btnFeedback").click(function() {

        event.preventDefault();

        if(checkFormAll($("#feedback")) == false)
        {
            MessageBox.Show('ERROR', 'It is empty in Message box.');
            return false;
        }

		var action = 'Feedback/ajax_setFeedback/';
		var form_data = {
            satisfaction: $("input[type=radio]:checked").val(),
            improvement: $("#txtMessage.improvement").val()
		};
        
        $.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response) {
                console.log(response);

                var responseJson = jQuery.parseJSON(response)[0];
				
                if(responseJson.success == true) {
                    $("#feedback").fadeOut();
                    $("#message").fadeIn();
				}
				else {
					alert("[Error] " + responseJson.error);
				}
			}
		});
		return false;
    });
    
    $(".btnOrder").click(function(event) {

        curOrderedjBtn = $(this);

        //event.preventDefault();
        var action = 'Delivery/ajaxOrder/';
        var form_data = {
            foodid: $(this).attr('foodid')
        };

        $.ajax({
            type: "POST",
            url: action,
            data: form_data,
            success: function(response)
            {
                console.log(response);
                var responseJson = jQuery.parseJSON(response)[0];
                
                if(responseJson.success == true)
                {
                    droppingAnimation(responseJson, $("#formLogin"), curOrderedjBtn, $(".s_menu .menu.member"), function(){ });
                }
                // Login failed
                else
                {
                    MessageBox.Show("Order fail", responseJson.error);
                }
            }
        });
        return false;
    });
}

</script>

</body>
</html>