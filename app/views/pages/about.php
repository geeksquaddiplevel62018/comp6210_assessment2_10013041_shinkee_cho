<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<?php
// Data from database
if($data['aboutus'] != null)
{
    $aboutus = $data['aboutus'];
    $cnt = count($aboutus);
    $CEO = '';
    for($i=0;$i<$cnt;$i++)
    {
        if($aboutus[$i]['name'] == 'ceo')
        {
            $CEO .= $aboutus[$i]['content'];
        }
        else if($aboutus[$i]['name'] == 'image')
        {
            switch($aboutus[$i]['note'])
            {
                case 'chief1':
                    $imgChef1 = $aboutus[$i]['content'];
                    break;
                case 'chief2':
                    $imgChef2 = $aboutus[$i]['content'];
                    break;
                case 'staff':
                    $imgStaff = $aboutus[$i]['content'];
                    break;
                case 'post':
                    $imgPoster = $aboutus[$i]['content'];
                    break;
            }
        }
        else if($aboutus[$i]['name'] == 'textChef')
        {
            switch($aboutus[$i]['note'])
            {
                case 'chief1':
                    $textChef1 = $aboutus[$i]['content'];
                    break;
                case 'chief2':
                    $textChef2 = $aboutus[$i]['content'];
                    break;
            }
        }
        else if($aboutus[$i]['name'] == 'nameChef')
        {
            switch($aboutus[$i]['note'])
            {
                case 'chief1':
                    $nameChef1 = $aboutus[$i]['content'];
                    break;
                case 'chief2':
                    $nameChef2 = $aboutus[$i]['content'];
                    break;
            }
        }
        else if($aboutus[$i]['name'] == 'titleChef')
        {
            switch($aboutus[$i]['note'])
            {
                case 'chief1':
                    $jobTitleChef1 = $aboutus[$i]['content'];
                    break;
                case 'chief2':
                    $jobTitleChef2 = $aboutus[$i]['content'];
                    break;
            }
        }
    }
}
?>


<div class="s_main">
    <div class="s_container">

        <!-- Social media icons -->
        <?php include(APPROOT . "/views/includes/socialMediaIcons.php"); ?>

        <!-- Slogan -->
        <div id="layerB" class="layer">
            <span class="slogan"><q class="q1"></q>Have <b>here</b>? No! Have <b>There!</b><q class="q3"></q></span>
        </div>
        
        <!-- About us -->
        <div id="layerD" class="layer">
            <h1>About us</h1>

            <!-- CEO -->
            <p><?php echo $CEO; ?></p>
        </div>
        
        <!-- Chef 1 -->
        <div id="layerE" class="layer">
            <p><q class="q1"></q><?php echo $textChef1; ?></p>
            <p><?php echo $nameChef1; ?>,<br><?php echo $jobTitleChef1; ?></p>
            <img src="<?php echo $imgChef1; ?>">
        </div>

        <!-- Chef 2 -->
        <div id="layerF" class="layer">
            <p><q class="q1"></q><?php echo $textChef2; ?></p>
            <p><?php echo $nameChef2; ?>,<br><?php echo $jobTitleChef2; ?></p>
            <img src="<?php echo $imgChef2; ?>">
        </div>

        <!-- Grand Open -->
        <div id="layerH" class="layer">
            <img src="<?php echo $imgPoster; ?>">
        </div>

        <!-- Staff photo -->
        <div id="layerI" class="layer">
            <img src="/imgs/staff.jpg">
        </div>

        <!-- Bottom -->
        <?php include(APPROOT . "/views/includes/bottom.php"); ?>

    </div>
</div>

<script>
    Layout.arrange("template2");
</script>

</body>
</html>