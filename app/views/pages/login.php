<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<div class="s_main">
    <div class="s_container">

        <!-- Social media icons -->
        <?php include(APPROOT . "/views/includes/socialMediaIcons.php"); ?>

        <!-- Recommanded Food -->
        <?php include(APPROOT . "/views/includes/recommendLayerB.php"); ?>

        <!-- Login form -->
        <div id="layerD" class="layer">
<?php if($data["isLoggedin"] == false): $hideOrNot = 'hide'; ?>
            <form id="formLogin" action="">
                <input id="email" type="email" name="email" placeholder="your@email.com">
                <input id="password" type="password" name="password" placeholder="Password">
                <input id="btnLogin" type="submit" value="Login">
                <span id="errorMsg" class="errorMsg"></span>
                <span>Not registered? <a href="/signup.html">Create an account</a></span>
            </form>
<?php else: $hideOrNot = 'show';  endif; ?>
            <div id="message" class="message <?php echo $hideOrNot; ?>">You logged in. Enjoy!</div>
        </div>
        
        <!-- Testimonial -->
        <?php include(APPROOT . "/views/includes/testimonial.php"); ?>
        
        <!-- Favorite Foods -->
        <div id="layerH" class="layer">
            <h1>Favorite Foods</h1>
            <table>
                <tr>
                <th>Food</th>
                <th></th>
                <th>Price</th>
                <th>Rating</th>
                <th>Order</th>
                </tr>
                
                 <!-- Favorites -->
                 <?php include(APPROOT . "/views/includes/favorites.php"); ?>

            </table>
        </div>

        <!-- Bottom -->
        <?php include(APPROOT . "/views/includes/bottom.php"); ?>

    </div>
</div>

<script>
    Layout.arrange("template4");
</script>

<script type="text/javascript">

function init()
{
	
    // Check Form input field
    $('input').keyup(function(){ checkForm($(this)); });
    
    // Ajax
    $("#btnLogin").click(function(event) {
        
        event.preventDefault();

        if(checkFormAll($("#formLogin")) == false)
        {
            return false;
        }

		var action = 'Member/ajax_login/';
		var form_data = {
			email: $("#email").val(),
			password: $("#password").val()
		};
        
        $.ajax({
			type: "POST",
			url: action,
			data: form_data,
            success: function(response)
            {
                console.log(response);
                var responseJson = jQuery.parseJSON(response)[0];
				
                if(responseJson.success == true)
                {
                    var loginMenu = $(".s_menu .menu.member");
                    droppingAnimation(responseJson, $("#formLogin"), $("#message"), loginMenu, changeToLogin);
                }
                // Login failed
                else
                {
                    MessageBox.Show("Login Failed", responseJson.error);
				}
			}
		});
		return false;
	});
}

function changeToLogin(responseJson)
{
    var menuContainer = $(".s_menu");
    var loginMenu = $(".s_menu .menu.login");
    var signupMenu = $(".s_menu .menu.signup");

    signupMenu.hide(0);
    loginMenu.hide(0, function(){
        var url = 'MyPage/';
        var name = responseJson["fname"];

        // Admin
        if(responseJson["level"] == "10")
        {
            console.log(typeof "10");
            url = 'Admin/';
            name += ' (ADMIN)';
        }

        $('<li class="info menu"><a href="'+url+'">'+name+'</a></li>').insertBefore(loginMenu);
        $('<li class="logout menu"><a href="Member/logout">Logout</a></li>').insertBefore(loginMenu);
    });
}
</script>


</body>
</html>