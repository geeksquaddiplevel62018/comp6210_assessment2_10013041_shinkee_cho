<!DOCTYPE html>
<html>

<!-- Header -->
<?php include(APPROOT . "/views/includes/head_tag.php"); ?>

<body>

<!-- Menu on top -->
<?php include(APPROOT . "/views/includes/menu.php"); ?>

<!-- -->
<div class="s_main">
    <div class="s_container">
        
        <!-- Social media icons -->
        <?php include(APPROOT . "/views/includes/socialMediaIcons.php"); ?>

        <!-- Recommanded Food -->
        <?php include(APPROOT . "/views/includes/recommendLayerB.php"); ?>

        <!-- Signup form -->
        <div id="layerD" class="layer">
            <form id="signup" action="#">
                <input id="fname" type="text" name="fname" placeholder="First Name">
                <input id="lname" type="text" name="lname" placeholder="Last Name">
                <input id="email" type="email" name="email" placeholder="your@email.com">
                <input id="password" type="password" name="password" placeholder="Password">
                <input id="confirmPassword" type="password" name="confirmPassword" placeholder="Confirm Password">
                
                <span>By creating an account, you agree to our <a class="terms_conditions"> terms & conditions.</a></span>

                <input id="btnSignup" type="submit" value="Create account">
            </form>
            
            <div id="message" class="message hide">Created your account!</div>
        </div>
        
        <!-- Testimonial -->
        <?php include(APPROOT . "/views/includes/testimonial.php"); ?>
        
        <!-- Favorite Foods -->
        <div id="layerH" class="layer">
            <h1>Favorite Foods</h1>
            <table>
                <tr>
                <th>Food</th>
                <th></th>
                <th>Price</th>
                <th>Rating</th>
                <th>Order</th>
                </tr>
                
                <!-- Favorites -->
                <?php include(APPROOT . "/views/includes/favorites.php"); ?>

            </table>
        </div>

        <!-- Bottom -->
        <?php include(APPROOT . "/views/includes/bottom.php"); ?>

    </div>
</div>

<!-- Terms & Conditions -->
<?php include(APPROOT . "/views/includes/terms.php"); ?>

<script>
    Layout.arrange("template4");
</script>

<script type="text/javascript">

function init()
{
    // Check Form input field
    $('input').keyup(function(){ checkForm($(this)); });


	$("#btnSignup").click(function(event) {

        event.preventDefault();

        if(checkFormAll($("#signup")) == false)
        {
            return false;
        }

		var action = 'Member/ajax_addMember/';
		var form_data = {
            fname: $("#fname").val(),
            lname: $("#lname").val(),
            email: $("#email").val(),
            password: $("#password").val(),
            confirmPassword: $("#confirmPassword").val()
		};
        
        $.ajax({
			type: "POST",
			url: action,
			data: form_data,
			success: function(response) {
                console.log(response);
                var responseJson = jQuery.parseJSON(response)[0];
                
				if(responseJson.success == true) {

                    var loginMenu = $(".s_menu .menu.member");
                    droppingAnimation(responseJson, $("#signup"), $("#message"), loginMenu, changeToLogin);
				}
				else {
					MessageBox.Show("[Error] ", responseJson.error);
				}
			}
		});
		return false;
	});
}

function changeToLogin(responseJson)
{
    var menuContainer = $(".s_menu");
    var loginMenu = $(".s_menu .menu.login");
    var signupMenu = $(".s_menu .menu.signup");

    signupMenu.hide(0);
    loginMenu.hide(0, function(){
        $('<li class="info menu"><a href="MyPage/">'+responseJson["fname"]+'</a></li>').insertBefore(loginMenu);
        $('<li class="logout menu"><a href="Member/logout">Logout</a></li>').insertBefore(loginMenu);
    });
}


</script>

</body>
</html>