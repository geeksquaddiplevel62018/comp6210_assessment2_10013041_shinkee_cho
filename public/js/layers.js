/****************************************
  Set layout
 ****************************************/
var obj_Layout = function()
{
    var that = this;

    var template;

    var responsiveState = 5;

    var layers =
    {
        A:{x:0, y:0, w:35, h:180},

        B_template1:{x:50, y:0, w:330, h:180},
        B_template2:{x:50, y:0, w:525, h:180},
        B_template3:{x:50, y:0, w:403, h:180},
        B_template4:{x:50, y:0, w:230, h:180},
        //B_Contactus:{x:50, y:0, w:525, h:180},
        
        C_template1:{x:395, y:0, w:180, h:180},
        C_template3:{x:468, y:0, w:452, h:180},

        D_template1:{x:590, y:0, w:330, h:275},
        D_template2:{x:590, y:0, w:330, h:570},
        D_template4:{x:295, y:0, w:625, h:570},

        E:{x:0, y:195, w:280, h:180},
        E_template3:{x:0, y:195, w:920, h:0},
        
        F_template1:{x:295, y:195, w:280, h:375},
        F_template2:{x:295, y:195, w:280, h:180},
        
        G:{x:590, y:290, w:330, h:280},
        
        H:{x:0, y:390, w:280, h:375},
        H_template3:{x:"none", y:"none", w:280, h:180},
        
        I_template2:{x:295, y:390, w:280, h:180},

        J:{x:295, y:585, w:625, h:180},
        J_template3:{x:"none", y:"none", w:625, h:180}
    };

    var layersInPage = 
    {
        template1:{"A":layers.A, "B":layers.B_template1, "C":layers.C_template1, "D":layers.D_template1, "E":layers.E, "F":layers.F_template1, "G":layers.G, "H":layers.H,                         "J":layers.J},
        template2:{"A":layers.A, "B":layers.B_template2,                         "D":layers.D_template2, "E":layers.E, "F":layers.F_template2,               "H":layers.H, "I":layers.I_template2, "J":layers.J},
        template3:{"A":layers.A, "B":layers.B_template3, "C":layers.C_template3,                         "E":layers.E_template3,                             "H":layers.H_template3,               "J":layers.J_template3},
        template4:{"A":layers.A, "B":layers.B_template4,                         "D":layers.D_template4, "E":layers.E,                          "H":layers.H,                                      "J":layers.J}
        //contactus:{"A":layers.A, "B":layers.B_Contactus, "C":layers.C, "D":layers.D, "E":layers.E, "F":layers.F, "G":layers.G, "H":layers.H, "I":layers.I, "J":layers.J},
        //login:{"A":layers.A, "B":layers.B, "C":layers.C, "D":layers.D, "E":layers.E, "F":layers.F, "G":layers.G, "H":layers.H, "I":layers.I, "J":layers.J},
        //template3:{"A":layers.A, "B":layers.B, "C":layers.C, "D":layers.D, "E":layers.E, "F":layers.F, "G":layers.G, "H":layers.H, "I":layers.I, "J":layers.J}
    };

    this.arrange = function(page)
    {
        that.template = page;

        for(layer in layersInPage[page])
        {
            $("#layer"+layer).css({
                left:layersInPage[page][layer].x,
                top:layersInPage[page][layer].y,
                height:layersInPage[page][layer].h,
                width:layersInPage[page][layer].w
            });
        }

        that.resize();
    };

    var getWidth = function(template, layer){ return layersInPage[template][layer].w; }
    var getHeight = function(template, layer){ return layersInPage[template][layer].h; }
    var getLeft = function(template, layer){ return layersInPage[template][layer].x; }
    var getTop = function(template, layer){ return layersInPage[template][layer].y; }

    var addLastColClass = function(nthCol)
    {
        $("div.item").removeAttr("style");
        $("div.item.colLast").removeClass("colLast").addClass("col");
        
        $("div.col-sm").removeAttr("style");
        $("div.col-sm.colLast").removeClass("colLast");

        var items = (nthCol == 3) ? $("div.col-sm") : $("div.item");
        for(var i=0; i<items.length;i++)
        {
            if(i%nthCol === (nthCol-1))
            {
                $(items[i]).addClass("colLast");
            }
        }
    }

    var addDummyLayer = function()
    {
        var count = 3 - ($("#layerE div.row div.col-sm").length % 3);

        if(count == 3){ return; }
        else
        {
            for(var i=0;i<count; i++)
            {
                $("#layerE div.row").append("<div class='col-sm dummy'></div>");
            }
        }
    }

    var removeDummyLayer = function()
    {
        $("#layerE div.row div.dummy").remove();
    }

 
    this.resize = function()
    {
        var template = that.template;
        var gap,
            win_W = $(window).width(),
            s_main_W = 920,
            cur_main_W = $("div.s_main").width(),
            
            layerA_L, layerA_T, layerA_W, layerA_H,
            layerB_L, layerB_T, layerB_W, layerB_H,
            layerC_L, layerC_T, layerC_W, layerC_H,
            layerD_L, layerD_T, layerD_W, layerD_H,
            layerE_L, layerE_T, layerE_W, layerE_H,
            item_W,
            layerF_L, layerF_T, layerF_W, layerF_H,
            layerG_L, layerG_T, layerG_W, layerG_H,
            layerH_L, layerH_T, layerH_W, layerH_H,
            layerI_L, layerI_T, layerI_W, layerI_H,
            layerJ_L, layerJ_T, layerJ_W, layerJ_H,
            layerJ_bg_W,
            layerJ_map_L,
            layerJ_contact_L;
        
        if(template === "template1")
        {
            layerC_L = getLeft(template, "C");
            layerC_T = getTop(template, "C");
            layerC_W = getWidth(template, "C");
            layerC_H = getHeight(template, "C");

            layerD_L = getLeft(template, "D");
            layerD_T = getTop(template, "D");
            layerD_W = getWidth(template, "D");
            layerD_H = getHeight(template, "D");

            layerF_L = getLeft(template, "F");  
            layerF_T = getTop(template, "F");
            layerF_W = getWidth(template, "F");
            layerF_H = getHeight(template, "F");

            layerG_L = getLeft(template, "G");
            layerG_T = getTop(template, "G");
            layerG_W = getWidth(template, "G");
            layerG_H = getHeight(template, "G");
        }
        else if(template === "template2")
        {
            layerD_L = getLeft(template, "D");
            layerD_T = getTop(template, "D");
            layerD_W = getWidth(template, "D");
            layerD_H = getHeight(template, "D");

            layerF_L = getLeft(template, "F");  
            layerF_T = getTop(template, "F");
            layerF_W = getWidth(template, "F");
            layerF_H = getHeight(template, "F");

            layerI_L = getLeft(template, "I");
            layerI_T = getTop(template, "I");
            layerI_W = getWidth(template, "I");
            layerI_H = getHeight(template, "I");
        }
        else if(template === "template3")
        {
            layerC_W = getWidth(template, "C");
            layerC_L = getLeft(template, "C");
            layerC_H = getHeight(template, "C");
            
            item_W = 219;
        }
        else if(template === "template4")
        {
            layerD_W = getWidth(template, "D");
            layerD_L = getLeft(template, "D");
            layerD_H = getHeight(template, "D");
        }

        layerA_L = getLeft(template, "A");
        layerA_T = getTop(template, "A");
        layerA_W = getWidth(template, "A");
        layerA_H = getHeight(template, "A");
        
        layerB_L = getLeft(template, "B");
        layerB_T = getTop(template, "B");
        layerB_W = getWidth(template, "B");
        layerB_H = getHeight(template, "B");

        layerE_L = getLeft(template, "E");
        layerE_T = getTop(template, "E");
        layerE_W = getWidth(template, "E");
        layerE_H = getHeight(template, "E");

        layerH_L = getLeft(template, "H");
        layerH_T = getTop(template, "H");
        layerH_W = getWidth(template, "H");
        layerH_H = getHeight(template, "H");

        layerJ_L = getLeft(template, "J");
        layerJ_T = getTop(template, "J");
        layerJ_W = getWidth(template, "J");
        layerJ_H = getHeight(template, "J");

        layerJ_bg_W = 250;
        layerJ_map_L = 250;
        layerJ_contact_L = 460;

        gap = s_main_W + 20 - win_W;    // 20 : padding

        //------------------
        // Col 2
        //------------------
        if(win_W <= 420)
        {
            if(responsiveState !== 2)
            {
                removeDummyLayer();
                addLastColClass(2);
                responsiveState = 2;
            }

            var w100 = s_main_W - gap;

            // s_main
            $("div.s_main").width(w100);

            if(template === "template1")
            {
                var top = 0;

                // Layer B
                $("#layerB").css({left:layerB_L, top:top, width:w100 - layerB_L});

                top += layerA_H+15;
                // Layer C
                $("#layerC").css({left:0, top:top, width:w100});
                
                top += layerC_H+15;
                // Layer F
                $("#layerF").css({left:0, top:top, width:w100});

                top += layerF_H+15;
                // Layer G
                $("#layerG").css({left:0, top:top, width:w100});

                top += layerG_H+15
                // Layer E
                $("#layerE").css({left:0, top:top, width:w100});

                top += layerE_H+15
                // Layer H
                $("#layerH").css({left:0, top:top, width:w100});

                top += layerH_H+15
                // Layer J
                $("#layerJ").css({left:0, top:top, width:w100});
            }
            else if(template === "template2")
            {
                var top = 0;

                // Layer B
                $("#layerB").css({left:layerB_L, top:top, width:w100 - layerB_L});

                top += layerA_H+15;
                // Layer F
                $("#layerF").css({left:0, top:top, width:w100});

                top += layerF_H+15;
                // Layer E
                $("#layerE").css({left:0, top:top, width:w100});

                top += layerE_H+15
                // Layer I
                $("#layerI").css({left:0, top:top, width:w100});

                top += layerI_H+15
                // Layer D
                $("#layerD").css({left:0, top:top, width:w100});

                top += layerD_H+15
                // Layer H
                $("#layerH").css({left:0, top:top, width:w100, height:"auto"});

                top += $("#layerH").height()+15
                // Layer J
                $("#layerJ").css({left:0, top:top, width:w100});
            }
            else if(template === "template3")
            {
                // Layer B
                $("#layerB").css({left:layerB_L, top:0, width:s_main_W - layerB_L - gap});

                // Layer C
                $("#layerC").css({left:0, top:195, width:w100});

                // Layer E
                $("#layerE").css({left:0, top:390, width:w100});
                $("div.item").css({width:(s_main_W-15)/2 - gap/2});

                // Layer H
                $("#layerH").css({left:0, top:0, width:w100});
            }
            else if(template === "template4")
            {
                // Layer B
                $("#layerB").css({left:layerB_L, top:0, width:s_main_W - layerB_L - gap});

                // Layer E
                $("#layerE").css({left:0, top:layerA_H + 15, width:w100});

                // Layer D
                $("#layerD").css({left:0, top:layerE_T + layerE_H + 15, width:w100});

                // Layer H
                $("#layerH").css({left:0, top:layerE_T + layerE_H + 15 + layerD_H + 15, width:(s_main_W-gap)});

                // Layer J
                $("#layerJ").css({left:0, top:layerE_T + layerE_H + 15 + layerD_H + 15 + layerH_H + 15, width:"100%"});
                $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
            }

            // Layer J
            $("#layerJ").css({width:"100%"});
            $("#layerJ div.bg").width(0);
            $("#layerJ div.gMap").css("left", 0);
            $("#layerJ div.contactus").css("left",layerJ_contact_L - 250);
        }
        //------------------
        // Col 3
        //------------------
        else if(win_W <= 690)
        {
            if(responsiveState !== 3)
            {
                addDummyLayer();
                addLastColClass(3);
                responsiveState = 3;
            }

            var w100 = s_main_W - gap;

            // s_main
            $("div.s_main").width(w100);

            if(template === "template1")
            {
                if(win_W <= 550)
                {
                    var top = 0;

                    // Layer B
                    $("#layerB").css({left:layerB_L, top:top, width:w100 - layerB_L});

                    top += layerA_H+15;
                    // Layer C
                    $("#layerC").css({left:0, top:top, width:w100});

                    top += layerC_H+15;
                    // Layer F
                    $("#layerF").css({left:0, top:top, width:w100});

                    top += layerF_H+15;
                    // Layer G
                    $("#layerG").css({left:0, top:top, width:w100, height:layerG_H});

                    top += layerG_H+15;
                    // Layer E
                    $("#layerE").css({left:0, top:top, width:w100});

                    top += layerE_H+15;
                    // Layer H
                    $("#layerH").css({left:0, top:top, width:w100});

                    top += layerH_H+15;
                    // Layer D
                    $("#layerD").css({left:0, top:top, width:w100});
                }
                else
                {
                    var top = 0, w1 = 0, w2 = 0, l = 0;
    
                    w1 = (w100-layerB_L)*(layerB_W/(layerB_W + 15 + layerC_W));
                    // Layer B
                    $("#layerB").css({left:layerB_L, top:top, width:w1});
                    w2 = (w100-layerB_L)*(layerC_W/(layerB_W + 15 + layerC_W));
                    // Layer C
                    $("#layerC").css({left:layerB_L + w1 + 15, top:top, width:w2});

                    top = layerE_T;
                    w1 = (w100-layerE_L)*(layerE_W/(layerE_W + 15 + layerF_W));
                    // Layer E
                    $("#layerE").css({left:0, top:top, width:w1});
                    w2 = (w100-layerE_L)*(layerF_W/(layerE_W + 15 + layerF_W));
                    l = layerE_L + w1 + 15;
                    // Layer F
                    $("#layerF").css({left:l, top:top, width:w2});

                    top += layerE_H + 15;
                    // Layer H
                    $("#layerH").css({left:0, top:top, width:w1});

                    top += layerH_H/2 + 15/2;
                    // Layer G
                    $("#layerG").css({left:l, top:top, width:w2, height:(layerH_H-15)/2});
    
                    top += (layerH_H-15)/2+15;
                    // Layer D
                    $("#layerD").css({left:0, top:top, width:w100, height:layerD_H});
                }

                top += layerD_H+15;

                // Layer J
                $("#layerJ").css({left:0, top:top, width:w100});
                $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
            }
            else if(template === "template2")
            {
                if(win_W <= 550)
                {
                    var top = 0;

                    // Layer B
                    $("#layerB").css({left:layerB_L, top:top, width:w100 - layerB_L});
    
                    top += layerA_H+15;                    
                    // Layer F
                    $("#layerF").css({left:0, top:top, width:w100});
    
                    top += layerF_H+15;    
                    // Layer E
                    $("#layerE").css({left:0, top:top, width:w100});
    
                    top += layerE_H+15
                    // Layer I
                    $("#layerI").css({left:0, top:top, width:w100});

                    top += layerI_H+15;
                    // Layer D
                    $("#layerD").css({left:0, top:top, width:w100});

                    top += layerD_H+15;
                    // Layer H
                    $("#layerH").css({left:0, top:top, width:w100, height:"auto"});                    

                    // Layer J
                    if($("#layerH").height() < 100)
                    {
                        top += $("#layerH").height() + 15;
                        
                        // Layer J
                        $("#layerJ").css({left:0, top:top, width:w100});
                        $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                        $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                        $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
                    }
                    else
                    {

                        setTimeout(function(){
                            top = $("#layerH").position().top + $("#layerH").height() + 15;

                            // Layer J
                            $("#layerJ").css({left:0, top:top, width:"100%"});
                            $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                            $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                            $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
                        }, 1000);
                    }
                }
                else
                {
                    var top = 0, w1 = 0, w2 = 0, l = 0;

                    // Layer B
                    $("#layerB").css({left:layerB_L, top:top, width:(w100-layerB_L)});

                    top = layerE_T;
                    w1 = (w100-layerE_L)*(layerE_W/(layerE_W + 15 + layerF_W));
                    // Layer E
                    $("#layerE").css({left:0, top:top, width:w1});
                    w2 = (w100-layerE_L)*(layerF_W/(layerE_W + 15 + layerF_W));
                    l = layerE_L + w1 + 15;
                    // Layer F
                    $("#layerF").css({left:l, top:top, width:w2});

                    top += layerF_H + 15;
                    // Layer I
                    $("#layerI").css({left:0, top:top, width:w1});
                    // Layer D
                    $("#layerD").css({left:l, top:top, width:w2, height:layerD_H});

                    top += layerI_H + 15;
                    // Layer H
                    $("#layerH").css({left:0, top:top, width:w1, height:layerH_H});
    
                    top += layerH_H+15;
                    // Layer J
                    $("#layerJ").css({left:0, top:top, width:w100});
                    $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                    $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                    $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
                }
            }
            else if(template === "template3")
            {
                if(win_W <= 570)
                {
                     // Layer B
                     $("#layerB").css({width : s_main_W - layerB_L - gap});

                     // Layer C
                     $("#layerC").css({left: 0, top:layerA_H + 15, width:w100});
 
                     // Layer E
                     $("#layerE").css({left:0, top:layerA_H + 15 + layerC_H + 15, width:layerE_W - gap});
                     $("div.col-sm").css({width:(s_main_W-30)/3 - gap/3});
                     $("div.item").css("width",(s_main_W-30)/3 - gap/3);
                     
                     // Layer H
                     $("#layerH").css({left:0, top:0, width:(s_main_W-gap)});
 
                // Layer B
                // $("#layerB").css({left:layerB_L, top:0, width:s_main_W - layerB_L - gap});

                // // Layer C
                // $("#layerC").css({left:0, top:195, width:(s_main_W-gap)});

                // // Layer E
                // $("#layerE").css({left:0, top:390, width:(s_main_W-gap)});
                // $("div.col-sm").css({width:(s_main_W-15)/2 - gap/2});
                // $("div.item").css({width:(s_main_W-15)/2 - gap/2});

                // // Layer H
                // $("#layerH").css({left:0, top:0, width:(s_main_W-gap)});

                     // Layer J
                     $("#layerJ").css({width:"100%"});
                     $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                     $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                     $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
                }
                else {
                    // Layer B
                    $("#layerB").width(layerB_W - gap/2);

                    // Layer C
                    $("#layerC").css({left: layerC_L - gap/2, top:0, width:layerC_W - gap/2});

                    // Layer E
                    $("#layerE").css({left:0, top:195, width:layerE_W - gap});
                    $("div.col-sm").css({width:(s_main_W-30)/3 - gap/3});
                    $("div.item").css("width",(s_main_W-30)/3 - gap/3);
                    
                    // Layer H
                    $("#layerH").css({left:0, top:0, width:(s_main_W-gap)});

                    // Layer J
                    $("#layerJ").css({width:"100%"});
                    $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                    $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                    $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
                }
            }
            else if(template === "template4")
            {
                
                //------------------
                // For template4
                //------------------
                if(win_W <= 550)
                {
                    // Layer B
                    $("#layerB").css({left:layerB_L, top:0, width:s_main_W - layerB_L - gap});

                    // Layer E
                    $("#layerE").css({left:0, top:layerA_H + 15, width:w100});

                    // Layer D
                    $("#layerD").css({left:0, top:layerE_T + layerE_H + 15, width:w100});

                    // Layer H
                    $("#layerH").css({left:0, top:layerE_T + layerE_H + 15 + layerD_H + 15, width:(s_main_W-gap)});

                    // Layer J
                    $("#layerJ").css({left:0, top:layerE_T + layerE_H + 15 + layerD_H + 15 + layerH_H + 15, width:"100%"});
                    $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                    $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                    $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
                }
                else
                {
                    var layerBE_W = (s_main_W - layerB_L - 15 - gap)/2;

                    // Layer B
                    $("#layerB").css({left:layerB_L, top:0, width:layerBE_W});

                    // Layer E
                    $("#layerE").css({left:layerB_L+layerBE_W + 15, top:0, width:layerBE_W});

                    // Layer D
                    $("#layerD").css({left:0, top:195, width:(w100)});

                    // Layer H
                    $("#layerH").css({left:0, top:layerE_T + layerD_H + 15, width:(s_main_W-gap)});

                    // Layer J
                    $("#layerJ").css({left:0, top:layerE_T + layerD_H + 15 + layerH_H + 15, width:"100%"});
                    $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                    $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                    $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
                }
            }

        }
        //------------------
        // Col 4
        //------------------
        else if(win_W <= 940)
        {
            if(responsiveState !== 4)
            {
                removeDummyLayer();
                addLastColClass(4);
                responsiveState = 4;
            }

            var w100 = s_main_W - gap;

            // s_main
            $("div.s_main").width(w100);

            if(template === "template1")
            {
                var top = 0, w1 = 0, w2 = 0, l = 0;
    
                w1 = (w100-layerB_L)*(layerB_W/(layerB_W + 15 + layerC_W));
                // Layer B
                $("#layerB").css({left:layerB_L, top:top, width:w1});
                w2 = (w100-layerB_L)*(layerC_W/(layerB_W + 15 + layerC_W));
                // Layer C
                $("#layerC").css({left:layerB_L + w1 + 15, top:top, width:w2});

                top = layerE_T;
                w1 = (w100-layerE_L)*(layerE_W/(layerE_W + 15 + layerF_W));
                // Layer E
                $("#layerE").css({left:0, top:top, width:w1});
                w2 = (w100-layerE_L)*(layerF_W/(layerE_W + 15 + layerF_W));
                l = layerE_L + w1 + 15;
                // Layer F
                $("#layerF").css({left:l, top:top, width:w2});

                top += layerE_H + 15;
                // Layer H
                $("#layerH").css({left:0, top:top, width:w1});

                top += layerH_H/2 + 15/2;
                // Layer G
                $("#layerG").css({left:l, top:top, width:w2, height:(layerH_H-15)/2});

                top += (layerH_H-15)/2+15;
                // Layer D
                $("#layerD").css({left:0, top:top, width:w100, height:layerD_H});

                top += layerD_H+15;
                // Layer J
                $("#layerJ").css({left:0, top:top, width:w100});
                $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
            }
            else if(template === "template2")
            {
                var top = 0, w1 = 0, w2 = 0, l = 0;

                // Layer B
                $("#layerB").css({left:layerB_L, top:top, width:(w100-layerB_L)});

                top = layerE_T;
                w1 = w100*(layerE_W/(layerE_W + 15 + layerF_W));
                w2 = w100*(layerF_W/(layerE_W + 15 + layerF_W));
                l = layerE_L + w1 + 15;
                // Layer E
                $("#layerE").css({left:0, top:top, width:w1});
                // Layer F
                $("#layerF").css({left:l, top:top, width:w2});

                top += layerF_H + 15;
                // Layer I
                $("#layerI").css({left:0, top:top, width:w1});
                // Layer D
                $("#layerD").css({left:l, top:top, width:w2, height:layerD_H});

                top += layerI_H + 15;
                // Layer H
                $("#layerH").css({left:0, top:top, width:w1, height:layerH_H});

                top += layerH_H+15;
                // Layer J
                $("#layerJ").css({left:0, top:top, width:w100});
                $("#layerJ div.bg").css({width:layerJ_bg_W - 690 + win_W});
                $("#layerJ div.gMap").css({left :layerJ_map_L - 690 + win_W});
                $("#layerJ div.contactus").css({left:layerJ_contact_L - 690 + win_W});
            
            }
            else if(template === "template3")
            {
                // Layer B
                $("#layerB").css({left:layerB_L, top:0, width:layerB_W - gap/2});

                // Layer C
                $("#layerC").css({left:layerC_L - gap/2, top:0, width:layerC_W - gap/2});

                // Layer E
                // 4 columns
                $("#layerE").css({left:0, top:layerE_T, width:layerE_W - gap});
                $("div.item").css("width", item_W - gap/4);
                $("div.item.min1px").css("width",item_W - gap/4 - 1);
                
                // Layer H
                $("#layerH").css({left:0, top:layerH_T, width:layerH_W});

                // Layer J
                $("#layerJ").css({left:layerJ_L, top:layerJ_T, width:layerJ_W - gap});
                $("#layerJ div.bg").width(layerJ_bg_W - gap);
                $("#layerJ div.gMap").css("left",layerJ_map_L - gap);
                $("#layerJ div.contactus").css("left",layerJ_contact_L - gap);
            }
            else if(template === "template4")
            {
                // Layer B
                $("#layerB").css({left:layerB_L, top:0, width:layerB_W});

                // Layer D
                $("#layerD").css({left:layerD_L, top:0, width:layerD_W - gap});

                // Layer E
                $("#layerE").css({left:0, top:layerE_T, width:layerE_W});

                // Layer H
                $("#layerH").css({left:0, top:layerH_T, width:layerH_W});

                // Layer J
                $("#layerJ").css({left:layerJ_L, top:layerJ_T, width:layerJ_W - gap});
                $("#layerJ div.bg").width(layerJ_bg_W - gap);
                $("#layerJ div.gMap").css("left",layerJ_map_L - gap);
                $("#layerJ div.contactus").css("left",layerJ_contact_L - gap);
            }

            
        }
        //------------------
        // Col 4
        //------------------
        else if(win_W > 940)
        {   
            if(responsiveState !== 5)
            {
                removeDummyLayer();
                addLastColClass(4);
                responsiveState = 5;
            }

            // s_main
            $("div.s_main").width(s_main_W);

            if(template === "template1")
            {
                // Layer B
                $("#layerB").css({left:layerB_L, top:layerB_T, width:layerB_W});

                // Layer C
                $("#layerC").css({left:layerC_L, top:layerC_T, width:layerC_H});

                // Layer D
                $("#layerD").css({left:layerD_L, top:layerD_T, width:layerD_W, height:layerD_H});

                // Layer E
                $("#layerE").css({left:layerE_L, top:layerE_T, width:layerE_W});

                // Layer F
                $("#layerF").css({left:layerF_L, top:layerF_T, width:layerF_W});

                // Layer H
                $("#layerH").css({left:layerH_L, top:layerH_T, width:layerH_W});

                // Layer G
                $("#layerG").css({left:layerG_L, top:layerG_T, width:layerG_W, height:layerG_H});

                // Layer J
                $("#layerJ").css({left:layerJ_L, top:layerJ_T, width:layerJ_W});
                $("#layerJ div.bg").css({width:layerJ_bg_W});
                $("#layerJ div.gMap").css({left :layerJ_map_L});
                $("#layerJ div.contactus").css({left:layerJ_contact_L});
            }
            else if(template === "template2")
            {
                // Layer B
                $("#layerB").css({left:layerB_L, top:layerB_T, width:layerB_W});

                // Layer D
                $("#layerD").css({left:layerD_L, top:layerD_T, width:layerD_W, height:layerD_H});

                // Layer E
                $("#layerE").css({left:layerE_L, top:layerE_T, width:layerE_W});

                // Layer F
                $("#layerF").css({left:layerF_L, top:layerF_T, width:layerF_W});

                // Layer H
                $("#layerH").css({left:layerH_L, top:layerH_T, width:layerH_W, height:layerH_H});

                // Layer I
                $("#layerI").css({left:layerI_L, top:layerI_T, width:layerI_W});

                // Layer J
                $("#layerJ").css({left:layerJ_L, top:layerJ_T, width:layerJ_W});
                $("#layerJ div.bg").css({width:layerJ_bg_W});
                $("#layerJ div.gMap").css({left :layerJ_map_L});
                $("#layerJ div.contactus").css({left:layerJ_contact_L});
            }
            else if(template === "template3")
            {
                // Layer B
                $("#layerB").css({left:layerB_L, top:0, width:layerB_W});

                // Layer C
                $("#layerC").css({left:layerC_L, top:0, width:layerC_W});
                
                // Layer E
                $("#layerE").css({left:0, top:layerE_T, width:layerE_W});
                $("div.item").css("width",219);
                $("div.item.min1px").css("width", item_W-1);
            }
            else if(template === "template4")
            {
                // Layer B
                $("#layerB").css({left:layerB_L, top:0, width:layerB_W});

                // Layer D
                $("#layerD").css({left:layerD_L, top:0, width:layerD_W});

                // Layer E
                $("#layerE").css({left:0, top:layerE_T, width:layerE_W});
            }

            // Layer H
            $("#layerH").css({left:0, top:layerH_T, width:layerH_W});

            // Layer J
            $("#layerJ").css({left:layerJ_L, top:layerJ_T, width:layerJ_W});
            $("#layerJ div.bg").width(layerJ_bg_W);
            $("#layerJ div.gMap").css("left",layerJ_map_L);
            $("#layerJ div.contactus").css("left",layerJ_contact_L);
            
        }
    }
};

var Layout = new obj_Layout();
