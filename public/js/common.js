//----------------------------------------
//  Form checker
//----------------------------------------
function checkFormAll(form)
{
    var jForm = $(form);
    var jInputs = jForm.find("input, select, textarea").not("[type=submit]");
    var validation = true;
    var errorField = '';

    jInputs.each(function(){
        var type = $(this).attr('type');
        validation &= checkForm($(this));
    });

    return validation;
}

function checkForm(target)
{
    var validation = true;
    var type = $(target).attr('type');

    // Empty
    if($(target).val().trim() == ""){ validation = false; }
    else
    {
        // Check
        switch(type)
        {
            case 'email':
                validation = validateEmail($(target).val());
                break;

            case 'number':
                validateNumber($(target).val());
                break;

            case 'text':
                break;    

            case 'selector':
                break;

            case 'password':
                // Password Field
                if($(target).val().length < 4)
                {
                    validation = false;
                }

                // Confirm Password Field
                if($(target).attr('name') == 'confirmPassword')
                {
                    validation = ($("#password").val() == $(target).val()) ? true : false;
                }

                break;

            default:
                return true;
        }
    }

    // Add or remove a class
    // Correct value
    if(validation)
    {
        $(target).removeClass('invalidation');
        $(".vaildationPopup."+$(target).attr('id')).remove();
        //console.log($(".vaildationPopup."+$(target).attr('id')));
    }
    // Wrong value
    else
    {
        $(target).addClass('invalidation');
        validationPopup(type, target);
    }

    return validation;
}

function validationPopup(type, target)
{
    var message = '';

    // Check
    switch(type)
    {
        case 'email':
            message = "Wrong email address! (abc@abc.abc)";
            break;

        case 'text':
            message = "It's empty";
            break;
        
        case 'number':
            message = "Wrong number input!";
            break;

        case 'selector':
            message = "Please select your purpose!";
            break;

        case 'password':
            // Password Field
            message = "Plase, type in charactors more than 4.";

            // Confirm Password Field
            if($(target).attr('name') == 'confirmPassword')
            {
                message = "Passwords are different.";
            }

            break;

        default:
            return true;
    }

    // exist
    if($(".vaildationPopup."+$(target).attr('id')).length == 0)
    {
        $("<div class='vaildationPopup "+ $(target).attr('id')+"'>"+message+"</div>").appendTo($('body')).css({
            left:$(target).offset().left,
            top:$(target).offset().top+$(target).height()+12
        }).fadeIn();
    }
    else
    {
        $(".vaildationPopup."+$(target).attr('id')).fadeIn();
    }
}


function validateEmail(email)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateWhitespace(string)
{
    var re = /\s/g;
    return re.test(string);
}

function validateNumber(number)
{
    var re= /^\d+$/;
    //var re = /^(?=.*\d)[\d ]+$/;
    return re.test(number);
}


//----------------------------------------
//  Message Box
//----------------------------------------
var MessageBox =
{
    Create:function(){
        
        if($("#messageBox").length > 0)
        {
            return $("#messageBox");
        }

        var strEl = "<div id='bgLayer' class='bgLayer'></div>";
            strEl += "<div id='messageBox' class='messageBox'>";
            strEl += "<ul><li><div id='messageTitle' class='titleBar'></div></li>";
            strEl += "<li><div id='messageBody' class='textArea'></div></li>";
            strEl += "<li><div id='messageFooter' class='btnClose'>Close</div></li>";
            strEl += "</ul></div>";

        var strCss = "<style>";
            
            strCss += "div.bgLayer";
            strCss += "{";
            strCss += "position: fixed; left:0; top:0; width:100%; height:100%;";
            strCss += "background:#aaa; opacity: .7; z-index: 2000;";
            strCss += "}";

            strCss += "div.bgLayer:hover";
            strCss += "{";
            strCss += "cursor:pointer;";
            strCss += "}";

            strCss += "div.messageBox";
            strCss += "{";
            strCss += "position: fixed; left:50%; top:50%;";
            strCss += "background:White; z-index: 2001;";
            strCss += "}";

            strCss += "div.messageBox ul,";
            strCss += "div.messageBox li";
            strCss += "{";
            strCss += "list-style-type: none; margin:0; padding:0px; ";
            strCss += "}";

            strCss += "div.messageBox li:nth-child(2)";
            strCss += "{";
            strCss += "padding:5%; ";
            strCss += "}";

            strCss += "div.messageBox li:last-child";
            strCss += "{";
            strCss += "padding:5%; ";
            strCss += "}";
            
            strCss += "div.messageBox .titleBar";
            strCss += "{";
            strCss += "width:100%; text-align:center; font-weight:600;";
            strCss += "padding:0; line-height:40px;";
            strCss += "color:#FFF; background:#8BBF5D;";
            strCss += "}";
            
            strCss += "div.messageBox .textArea";
            strCss += "{";
            strCss += "width:100%;";
            strCss += "height:80px;";
            strCss += "}";
            
            strCss += "div.messageBox .btnClose";
            strCss += "{";
            strCss += "width:100%; border-radius: 3px;";
            strCss += "height:40px; line-height:40px; text-align:center; ";
            strCss += "background:#8BBF5D; ";
            strCss += "color:#fff;";
            strCss += "}";

            strCss += "div.messageBox .btnClose:hover";
            strCss += "{ cursor:pointer; }";


            strCss += "</style>";

        $(strCss).appendTo($('body'));
        $(strEl).appendTo($('body'));

        MessageBox.SetEvent();
    },

    GetjBgLayer: function(){ return $("#bgLayer"); },
    GetjPopup: function(){ return $("#messageBox"); },
    GetjTitle: function(){ return $("#messageTitle"); },
    GetjBody: function(){ return $("#messageBody"); },
    GetjButton: function(){ return $("#messageFooter"); },
    RedirectURL:null,

    SetEvent:function(){
        MessageBox.GetjBgLayer().click(function(){
            $(this).fadeOut(1000, 'easeInOutBack', function(){ });
            MessageBox.GetjPopup().fadeOut(1000, 'easeInOutBack', function(){ });
        });

        MessageBox.GetjButton().click(function(){
            MessageBox.GetjBgLayer().fadeOut(1000, 'easeInOutBack', function(){ });
            MessageBox.GetjPopup().fadeOut(1000, 'easeInOutBack', function(){
                if(MessageBox.RedirectURL != null)
                {
                    location.href = MessageBox.RedirectURL;
                }
            });
        });
    },
    SetTitle:function(title){
        MessageBox.GetjTitle().html(title);
    },
    SetMessage:function(message){
        MessageBox.GetjBody().html(message);
    },
    Resize:function()
    {
        var winW = $(window).width();
        var winH = $(window).height();

        if(winW > 420)
        {
            MessageBox.GetjPopup().css({
                width:'400px',
                marginLeft:'-200px',
                marginTop:-MessageBox.GetjPopup().height()/2,       
            })
        }
        else
        {
            MessageBox.GetjPopup().css({
                width:'90%',
                marginLeft:'-45%',
                marginTop:-MessageBox.GetjPopup().height()/2,  
            })
        }
    },

    Show : function(title, message, redirectURL = null){

        MessageBox.RedirectURL = redirectURL;
        MessageBox.Create();
        MessageBox.SetTitle(title);
        MessageBox.SetMessage(message);
        MessageBox.Resize();
        MessageBox.GetjBgLayer().fadeIn();
        MessageBox.GetjPopup().fadeIn();
    }
};


//----------------------------------------
//  Animation
//----------------------------------------
function droppingAnimation(responseJson, formToHide, fromTarget, toTarget, callback)
{
    if(formToHide != null){ formToHide.hide(0); }

    var fromTargetClone = fromTarget.clone();
        
    fromTargetClone.attr("id","");
    fromTargetClone.removeClass("hide");
    fromTargetClone.html($("#fname").html());

    fromTarget.removeClass('hide');
    fromTarget.fadeIn();

    
    // fromTargetClone.css({
    //     left:form.offset().left + form.offset().width/2,
    //     top:form.offset().top + form.offset().height/2
    // });
    fromTargetClone.css({
        position:"absolute",
        zindex:100,
        color:fromTarget.css('color'),
        margin:0,
        background:fromTarget.css('background'),
        textDecoration:'none',
        border:0,
        borderRadius:fromTarget.css('border-radius'),
        left:fromTarget.offset().left,
        top:fromTarget.offset().top,
        width:fromTarget.width(),
        height:fromTarget.height()
    });

    $("body").append(fromTargetClone);
    
    fromTargetClone.animate({
        left:toTarget.offset().left + toTarget.width()/2,
        top:toTarget.offset().top + toTarget.height()/2,
        width:2, height:2,padding:0
    },
    1000,
    'easeInOutBack',
    function(){
        $(this).remove();
        
        if(callback != null)
        {
            callback(responseJson);
        }
    });
}

//----------------------------------------
// Social Media Menu Icon Events
//----------------------------------------
function menuIconEvents(event)
{
    location.href = $(event.target).attr('href');
}

//----------------------------------------
//  Start point
//----------------------------------------
$(document).ready(function(){

    // Resize window
    if(typeof Layout != "undefined")
    {
        $(window).resize(Layout.resize);
    }
    
    // Mobile Menu (Touch event)
    // $("ul.s_menu").click(function() {
    //     $("ul.s_menu li:not(.menuTab)").toggle();
    // });

    // Social Media Menu Icon Events
    $(".menuIcon").click(menuIconEvents);

    // Click terms and conditions in signup page
    $(".terms_conditions").click(function(){
        $("#terms").css({display:"block"});
    });

    // Terms & Conditions button event
    $("#terms").click(function(){ $(this).css({display:"none"}); });

    // Call init function
    if(typeof init === "function")
    {
        init();
    }
});